package com.example.camera.utils;

import android.content.Context;

import java.io.File;

import static com.example.camera.utils.Constants.dirEffects;
import static com.example.camera.utils.Constants.dirFrames;

public class MyFileManager {
    private static MyFileManager myFileManager;
    public static MyFileManager getInstance(){
        if (myFileManager == null){
            myFileManager = new MyFileManager();
        }
        return myFileManager;
    }

    public File getFrameFiles(Context context){
        File file = new File(context.getExternalFilesDir(dirFrames).getAbsolutePath());
        if (!file.exists()){
            file.mkdirs();
        }
        return file;
    }

    public File getEffectFiles(Context context){
        File file = new File(context.getExternalFilesDir(dirEffects).getAbsolutePath());
        if (!file.exists()){
            file.mkdirs();
        }
        return file;
    }

    public File getEffectCategory(Context context, String category){
        File file = new File(getEffectFiles(context), category);
        if (!file.exists()){
            file.mkdirs();
        }
        return file;
    }

    public File getFrameCategory(Context context, String category){
        File file = new File(getEffectFiles(context), category);
        if (!file.exists()){
            file.mkdirs();
        }
        return file;
    }
}
