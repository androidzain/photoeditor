package com.example.camera.utils;

import android.media.Image;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.camera.R;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MyHolder extends RecyclerView.ViewHolder {
    public ImageView imageView;
    public ImageView selected;
    public RecyclerView recyclerView;
    public MyHolder(@NonNull View itemView) {
        super(itemView);
        imageView = itemView.findViewById(R.id.frameImage);
        recyclerView = itemView.findViewById(R.id.child);
        selected = itemView.findViewById(R.id.selected);
//        resourceImage = itemView.findViewById(R.id.resourceImage);
//        textView = itemView.findViewById(R.id.text);
    }
}
