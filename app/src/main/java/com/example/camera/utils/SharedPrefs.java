package com.example.camera.utils;

import android.content.Context;
import android.content.SharedPreferences;


public class SharedPrefs {
    public SharedPreferences preferences;
    private static SharedPrefs sharedPrefs;
    private SharedPrefs(Context context){
        if (preferences == null){
            preferences = context.getSharedPreferences(Constants.prefs, Context.MODE_PRIVATE);
        }
    }
    public synchronized static SharedPrefs getInstance(Context context){
        if (sharedPrefs == null){
            sharedPrefs = new SharedPrefs(context);
        }
        return sharedPrefs;
    }

    public SharedPreferences getPreferences(){
        return preferences;
    }

    public void savePref(String key, Boolean value){
        preferences.edit().putBoolean(key, value).apply();
    }
    public Boolean getBooleanPref(String key){
        return preferences.getBoolean(key, false);
    }
    public Boolean getBooleanPref(String key, Boolean b){
        return preferences.getBoolean(key, b);
    }
    public String getStringPref(String key){
        return preferences.getString(key, "");
    }
}
