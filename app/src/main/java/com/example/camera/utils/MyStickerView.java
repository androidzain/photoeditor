package com.example.camera.utils;

import android.content.Context;
import android.util.AttributeSet;

import com.xiaopo.flying.sticker.Sticker;
import com.xiaopo.flying.sticker.StickerView;

import androidx.annotation.NonNull;

public class MyStickerView extends StickerView implements StickerView.OnStickerOperationListener {
    public MyStickerView(Context context) {
        super(context);
    }

    public MyStickerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyStickerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void onStickerAdded(@NonNull Sticker sticker) {

    }

    @Override
    public void onStickerClicked(@NonNull Sticker sticker) {

    }

    @Override
    public void onStickerDeleted(@NonNull Sticker sticker) {

    }

    @Override
    public void onStickerDragFinished(@NonNull Sticker sticker) {

    }

    @Override
    public void onStickerTouchedDown(@NonNull Sticker sticker) {

    }

    @Override
    public void onStickerZoomFinished(@NonNull Sticker sticker) {

    }

    @Override
    public void onStickerFlipped(@NonNull Sticker sticker) {

    }

    @Override
    public void onStickerDoubleTapped(@NonNull Sticker sticker) {

    }
}
