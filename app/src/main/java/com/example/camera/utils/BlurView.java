package com.example.camera.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.graphics.PorterDuff;
import android.util.Log;


import com.example.camera.R;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;

public class BlurView extends AppCompatImageView {
    private Bitmap bitmap;
    private Paint transparentPaint;
    private Paint p = new Paint();
    private Paint paint;
    private Canvas temp;
    public BlurView(Context context) {
        super(context);
    }

    public BlurView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public BlurView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
    private Bitmap b;
    private int radius;
    public void setBitmap(Bitmap bitmap, int radius){
        Log.v("radius", String.valueOf(radius));
        this.radius = radius;
        this.radius = 200;
        setWillNotDraw(false);
        setLayerType(LAYER_TYPE_HARDWARE, null);
        transparentPaint = new Paint();
        transparentPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        this.bitmap = bitmap;
//        b = bitmap.copy(Bitmap.Config.ARGB_8888,true);
        b = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), true);
        temp = new Canvas(b);
    }
    public void setRadius(int radius){
        this.radius = radius;
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
//        temp.drawCircle(  getWidth()/2 , getHeight()/2,radius, transparentPaint);
        canvas.drawBitmap(b, 0, 0, p);
    }
}
