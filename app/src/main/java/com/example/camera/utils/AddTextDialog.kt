package com.example.camera.utils

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.os.Bundle
import android.text.SpannableString
import android.view.View
import android.widget.SeekBar
import androidx.fragment.app.FragmentActivity
import com.example.camera.R
import com.jaredrummler.android.colorpicker.ColorPickerDialog
import com.jaredrummler.android.colorpicker.ColorPickerDialog.*
import kotlinx.android.synthetic.main.add_text_dialog.*
import com.otaliastudios.cameraview.overlay.OverlayLayout
import android.graphics.Typeface
import android.text.style.StyleSpan
import android.text.style.UnderlineSpan
import android.util.Log
import android.widget.Toast
import com.jaredrummler.android.colorpicker.ColorPickerDialogListener

class AddTextDialog(private val activity: Activity, private val style: Int, private val listener: DialogListener): Dialog(activity, style){

    private val DIALOG_ID = 0
    private var textSize = 10f
    private var isBold = false
    private var isItalic = false
    private var isUnderline = false
    private var colourCode = 323
    private var spanString : SpannableString? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_text_dialog)
        seek.max = 50
        val window = getWindow()
        window.setLayout(OverlayLayout.LayoutParams.MATCH_PARENT, OverlayLayout.LayoutParams.WRAP_CONTENT)
        seek.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(seekbar: SeekBar?, size: Int, p2: Boolean) {
                input.textSize = size.toFloat()
                textSize = size.toFloat()
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {

            }

            override fun onStopTrackingTouch(p0: SeekBar?) {

            }
        })
        size.setOnClickListener {
            if (seek.visibility == View.VISIBLE){
                seek.visibility = View.GONE
                return@setOnClickListener
            }
            seek.visibility = View.VISIBLE
        }
        color.setOnClickListener {
            newBuilder()
                    .setDialogType(TYPE_CUSTOM)
                    .setAllowPresets(false)
                    .setDialogId(DIALOG_ID)
                    .setColor(Color.BLACK)
                    .setShowAlphaSlider(true)
                    .show(activity as FragmentActivity)
        }
        okay.setOnClickListener {
            if (input.text.toString() != ""){
                listener.okay(input.text.toString(), colourCode, textSize.toInt(), isBold, isUnderline, isItalic)
                dismiss()
            }else{
                Toast.makeText(activity, "Enter Text First", Toast.LENGTH_SHORT).show()
            }
        }
        cancel.setOnClickListener {
            dismiss()
        }
//        bold.setOnClickListener {
//            isBold = !isBold
//            if (isBold){
//                bold.setBackgroundResource(R.drawable.border)
//            }else{
//                try {
//                    bold.setBackgroundResource(0)
//                } catch (e: Exception) {
//                }
//            }
//        }
//        italic.setOnClickListener {
//            isItalic = !isItalic
//            if (isItalic){
//                italic.setBackgroundResource(R.drawable.border)
//            }else{
//                try {
//                    italic.setBackgroundResource(0)
//                } catch (e: Exception) {
//                }
//            }
//        }
//        underline.setOnClickListener {
//            isUnderline = !isUnderline
//            if (isUnderline){
//                underline.setBackgroundResource(R.drawable.border)
//            }else{
//                try {
//                    underline.setBackgroundResource(0)
//                } catch (e: Exception) {
//                    e.printStackTrace()
//                }
//            }
//        }
    }

    public interface DialogListener{
        fun okay(text: String, color: Int, size: Int, isBold: Boolean, isUnderline: Boolean, isItalic: Boolean)
    }

}