package com.example.camera.ads

import android.app.Activity
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd

class InterstitialAds {
    companion object{
        private var interstitialAd: InterstitialAd? = null
        fun loadAd(activity: Activity,strId: String){
            interstitialAd = InterstitialAd(activity)
            val request = AdRequest.Builder().build()
            interstitialAd!!.adUnitId = strId
            interstitialAd!!.loadAd(request)
        }

        fun isAdLoaded(activity: Activity, id: String): Boolean{
            if (interstitialAd == null){
                loadAd(activity, id)
                return false
            }
            if (!interstitialAd!!.isLoaded){
                loadAd(activity, id)
                return false
            }
            return true
        }

        fun showAd(activity: Activity, id: String){
            interstitialAd!!.show()
            loadAd(activity, id)
        }
    }
}