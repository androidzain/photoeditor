package com.example.camera.ads

import android.app.Activity
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import com.example.camera.R
import com.google.android.gms.ads.AdLoader
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.VideoController
import com.google.android.gms.ads.VideoOptions
import com.google.android.gms.ads.formats.NativeAdOptions
import com.google.android.gms.ads.formats.UnifiedNativeAd
import com.google.android.gms.ads.formats.UnifiedNativeAdView

class NativeAds {
    companion object {

        fun loadAdmobNativeAd(mContext: Context, frameLayout: FrameLayout, adId:  String) {
            var admobNativeAd: UnifiedNativeAd? = null
            val inflater = LayoutInflater.from(mContext)
            val builder = AdLoader.Builder(
                    mContext,
                    adId
            )
            builder.forUnifiedNativeAd { unifiedNativeAd ->
                if (admobNativeAd != null) {
                    admobNativeAd!!.destroy()
                }
                try {
                    admobNativeAd = unifiedNativeAd
                    val adView =  //actually card view of fb ad
                            inflater.inflate(R.layout.ad_unified, null) as UnifiedNativeAdView
                    populateUnifiedNativeAdView(unifiedNativeAd, adView)
                    frameLayout.removeAllViews()
                    frameLayout.addView(adView)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            val videoOptions = VideoOptions.Builder()
                    .setStartMuted(true/*startVideoAdsMuted.isChecked()*/)
                    .build()

            val adOptions = NativeAdOptions.Builder()
                    .setVideoOptions(videoOptions)
                    .build()

            builder.withNativeAdOptions(adOptions)
            val adLoader = builder.withAdListener(object : com.google.android.gms.ads.AdListener() {
                override fun onAdFailedToLoad(errorCode: Int) {
                    Log.e("NATIVE_ADMOB", "onAdFailedToLoad: NATIVE ADMOB ----------")
                }
            }).build()

            adLoader.loadAd(AdRequest.Builder().build())
        }

        private fun populateUnifiedNativeAdView(nativeAd: UnifiedNativeAd, adView: UnifiedNativeAdView) {
            Log.e("NATIVE_ADMOB", "populateUnifiedNativeAdView: " + (true))
            val mediaView = adView.findViewById<com.google.android.gms.ads.formats.MediaView>(R.id.ad_media)
            adView.mediaView = mediaView

            // Set other ad assets.
            adView.headlineView = adView.findViewById(R.id.ad_headline)
            adView.bodyView = adView.findViewById(R.id.ad_body)
            adView.callToActionView = adView.findViewById(R.id.ad_call_to_action)
            adView.iconView = adView.findViewById(R.id.ad_app_icon)
            adView.priceView = adView.findViewById(R.id.ad_price)
            adView.starRatingView = adView.findViewById(R.id.ad_stars)
            adView.storeView = adView.findViewById(R.id.ad_store)
            adView.advertiserView = adView.findViewById(R.id.ad_advertiser)
            (adView.headlineView as TextView).text = nativeAd.headline
            if (nativeAd.body == null) {
                adView.bodyView.visibility = View.INVISIBLE
            } else {
                adView.bodyView.visibility = View.VISIBLE
                (adView.bodyView as TextView).text = nativeAd.body
            }

            if (nativeAd.callToAction == null) {
                adView.callToActionView.visibility = View.INVISIBLE
            } else {
                adView.callToActionView.visibility = View.VISIBLE
                (adView.callToActionView as TextView).text = nativeAd.callToAction
            }

            if (nativeAd.icon == null) {
                adView.iconView.visibility = View.GONE
            } else {
                (adView.iconView as ImageView).setImageDrawable(
                        nativeAd.icon.drawable
                )
                adView.iconView.visibility = View.VISIBLE
            }

            if (nativeAd.price == null) {
                adView.priceView.visibility = View.INVISIBLE
            } else {
                adView.priceView.visibility = View.VISIBLE
                (adView.priceView as TextView).text = nativeAd.price
            }

            if (nativeAd.store == null) {
                adView.storeView.visibility = View.INVISIBLE
            } else {
                adView.storeView.visibility = View.VISIBLE
                (adView.storeView as TextView).text = nativeAd.store
            }

            if (nativeAd.starRating == null) {
                adView.starRatingView.visibility = View.INVISIBLE
            } else {
                (adView.starRatingView as RatingBar).rating = nativeAd.starRating!!.toFloat()
                adView.starRatingView.visibility = View.VISIBLE
            }

            if (nativeAd.advertiser == null) {
                adView.advertiserView.visibility = View.INVISIBLE
            } else {
                (adView.advertiserView as TextView).text = nativeAd.advertiser
                adView.advertiserView.visibility = View.VISIBLE
            }
            adView.setNativeAd(nativeAd)
            val vc = nativeAd.videoController

            if (vc.hasVideoContent()) {
                vc.videoLifecycleCallbacks = object : VideoController.VideoLifecycleCallbacks() {
                }
            } else {
            }
        }
    }
}