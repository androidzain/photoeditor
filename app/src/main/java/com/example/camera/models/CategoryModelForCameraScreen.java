package com.example.camera.models;

import java.io.File;
import java.util.ArrayList;

public class CategoryModelForCameraScreen {
    File filePath;
    int id;
    private ArrayList<File> listChild;
    private boolean isSelected;
    public ArrayList<File> getListChild() {
        return listChild;
    }

    public void setListChild(ArrayList<File> listChild) {
        this.listChild = listChild;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public CategoryModelForCameraScreen(File filePath, int id, boolean isSelected) {
        this.filePath = filePath;
        this.id = id;
        this.isSelected = isSelected;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public File getFilePath() {
        return filePath;
    }

    public int getId() {
        return id;
    }
}
