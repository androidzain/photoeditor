package com.example.camera;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.example.camera.Activities.CategoriesActivity;
import com.example.camera.Activities.MyTestingBlur;
import com.example.camera.Activities.PreviewActivity;
import com.example.camera.Adaptors.AdapterResources;
import com.example.camera.ads.NativeAds;
import com.example.camera.listeners.RecyclerItemClickListener;
import com.example.camera.utils.Constants;
import com.example.camera.utils.SharedPrefs;
import com.github.gabrielbb.cutout.CutOut;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.otaliastudios.cameraview.PictureResult;
import com.max.crop.mc.CropImageView;
import com.xiaopo.flying.sticker.DrawableSticker;
import com.xiaopo.flying.sticker.Sticker;
import com.xiaopo.flying.sticker.TextSticker;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

public abstract class BaseActivity extends AppCompatActivity implements View.OnClickListener, RecyclerItemClickListener.RecyclerListener {

    public RecyclerView recyclerView;
    public RecyclerView recyclerFramesAndEffects;
    public AdapterResources adapterResources;
    public ImageView recyclerBack;
    public boolean isInCrop = false;
    public boolean isInRecycler = false;
    public CURRENT_TOOL curTool;
    public ImageView previewImage;
    public ImageView effectImageView, frameImageView;
    public CropImageView cropImageView;
    public ImageView undo, redo;
    public boolean isTimerEnabled = false;
    public long time;
    public final int blurCode = 439;

    public int[] mirrors = {R.drawable.mirror_1, R.drawable.mirror_2, R.drawable.mirror_3, R.drawable.mirror_4, R.drawable.mirror_5,
            R.drawable.mirror_6, R.drawable.mirror_7};
    public int[] crops = {R.drawable.free_crop, R.drawable.crop_1, R.drawable.crop_one_one, R.drawable.crop_two_three,
            R.drawable.crop_three_two, R.drawable.crop_three_four};
    public int[] effectsList = {R.drawable.galaxy, R.drawable.seasons, R.drawable.clouds, R.drawable.colors, R.drawable.trees,
            R.drawable.tea, R.drawable.christmas};
    public int[] flips = {R.drawable.flip_up, R.drawable.flip_right, R.drawable.flip_down, R.drawable.flip_down};

    private ArrayList<ModelUndoRedo> undoList = new ArrayList<>();
    private ArrayList<ModelUndoRedo> redoList = new ArrayList<>();

    public abstract void initializeResources();

    protected void addIntoUndo(ModelUndoRedo item){
        undoList.add(item);
        if (undoList.size() > 0){
            undo.setEnabled(true);
            undo.setAlpha(1f);
        } else {
            undo.setEnabled(false);
            undo.setAlpha(0.5f);
        }
        redoList.clear();
        redo.setEnabled(false);
        redo.setAlpha(0.5f);
    }

    protected ModelUndoRedo undo(){
        ModelUndoRedo modelUndoRedo = undoList.remove(undoList.size() - 1);
//        redoList.add(modelUndoRedo);
        if (undoList.size() < 1){
            undo.setEnabled(false);
            undo.setAlpha(0.5f);
        }
        redo.setEnabled(true);
        redo.setAlpha(1f);
        return modelUndoRedo;
    }

    public void addRedo(ModelUndoRedo model){
        redoList.add(model);
    }
    public void addUndo(ModelUndoRedo model){
        undoList.add(model);
    }

    protected ModelUndoRedo redo(){
        ModelUndoRedo modelUndoRedo = redoList.remove(redoList.size() - 1);
//        undoList.add(modelUndoRedo);
        if (redoList.size() < 1){
            redo.setEnabled(false);
            redo.setAlpha(0.5f);
        }
        undo.setEnabled(true);
        undo.setAlpha(1f);
        return modelUndoRedo;
    }

    public class ModelUndoRedo{
        private Bitmap imageBitmap;
        private Bitmap effectBitmap;
        private Bitmap frameBitmap;
        private Sticker sticker;
        private TextSticker textSticker;

        public ModelUndoRedo(Bitmap imageBitmap,Bitmap effectBitmap,Bitmap frameBitmap, Sticker sticker, TextSticker textSticker) {
            this.imageBitmap = imageBitmap;
            this.effectBitmap = effectBitmap;
            this.frameBitmap = frameBitmap;
            this.sticker = sticker;
            this.textSticker = textSticker;
        }

        public Bitmap getImageBitmap() {
            return imageBitmap;
        }

        public Sticker getSticker() {
            return sticker;
        }

        public TextSticker getTextSticker() {
            return textSticker;
        }

        public Bitmap getEffectBitmap() {
            return effectBitmap;
        }

        public Bitmap getFrameBitmap() {
            return frameBitmap;
        }
    }

    public Bitmap getVerticalReflectionFullImage(Bitmap originalImage) {
        Bitmap bitmap;
        int width1 = originalImage.getWidth();
        int height1 = originalImage.getHeight();
        Log.v("widthEarlier", ""+width1+ " heightEarlier: " + height1);
        // This will not scale but will flip on the Y axis
        Matrix matrix = new Matrix();
        matrix.preScale(-1.0f, 1.0f);

        // Create a Bitmap with the flip matix applied to it.
        // We only want the bottom half of the image
        Bitmap reflectionImage = Bitmap.createBitmap(originalImage, 0,
                0, width1, height1, matrix, false);


        //this image will be shown on upper side of the mirror
        Bitmap halfImage = Bitmap.createBitmap(originalImage, 0,
                0, width1, height1, null, false);

        int width = halfImage.getWidth() * 2;
        int height = halfImage.getHeight();
        bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        Canvas comboImage = new Canvas(bitmap);

        comboImage.drawBitmap(halfImage, 0f, 0f, null);
        comboImage.drawBitmap(reflectionImage, halfImage.getWidth(), 0f, null);
        return bitmap;
    }

    public Bitmap getVerticalReflection(Bitmap originalImage) {
        Bitmap bitmap;
        int width1 = originalImage.getWidth();
        int height1 = originalImage.getHeight();
        Log.v("widthEarlier", ""+width1+ " heightEarlier: " + height1);


        // This will not scale but will flip on the Y axis
        Matrix matrix = new Matrix();
        matrix.preScale(-1.0f, 1.0f);

        // Create a Bitmap with the flip matix applied to it.
        // We only want the bottom half of the image
        Bitmap reflectionImage = Bitmap.createBitmap(originalImage, 0,
                0, width1 / 2, height1, matrix, false);


        //this image will be shown on upper side of the mirror
        Bitmap halfImage = Bitmap.createBitmap(originalImage, 0,
                0, width1 / 2, height1, null, false);

        int width = halfImage.getWidth() * 2;
        int height = halfImage.getHeight();
        bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Log.v("widthLater", ""+width+ " heightLater: " + height);


        Canvas comboImage = new Canvas(bitmap);

        comboImage.drawBitmap(halfImage, 0f, 0f, null);
        comboImage.drawBitmap(reflectionImage, halfImage.getWidth(), 0f, null);
        return bitmap;
    }
    public Bitmap getHalfUpAndDownMirror(Bitmap originalImage, int reflectionGap) {


        int width1 = originalImage.getWidth();
        int height1 = originalImage.getHeight();
        Log.v("widthEarlier", ""+width1+ " heightEarlier: " + height1);
        // This will not scale but will flip on the Y axis
        Matrix matrix = new Matrix();
        matrix.preScale(1, -1);
        // Create a Bitmap with the flip matix applied to it.
        // We only want the bottom half of the image
        Bitmap reflectionImage = Bitmap.createBitmap(originalImage, 0,
                height1 / 2, width1, height1 / 2, matrix, false);


        //this image will be shown on upper side of the mirror
        Bitmap halfImage = Bitmap.createBitmap(originalImage, 0,
                height1 / 2, width1, height1 / 2, null, false);

        int width = halfImage.getWidth();
        int height = halfImage.getHeight();

        // Create a new bitmap with same width but taller to fit reflection

        Bitmap bitmapWithReflection = Bitmap.createBitmap(width1,
                (height1), Bitmap.Config.ARGB_8888);
        // Create a new Canvas with the bitmap that's big enough for
        // the image plus gap plus reflection
        Log.v("widthLater", ""+bitmapWithReflection.getWidth()+ " heightLater: " + bitmapWithReflection.getHeight());
        Canvas canvas = new Canvas(bitmapWithReflection);
        // Draw in the original image
        canvas.drawBitmap(halfImage, 0, 0, null);
        // Draw in the gap
        Paint deafaultPaint = new Paint();
        // deafaultPaint.setColor(0xffffffff);

        canvas.drawRect(0, height, width, height + reflectionGap, deafaultPaint);

        canvas.drawBitmap(reflectionImage, 0, height + reflectionGap, null);

        Paint paint = new Paint();

        // LinearGradient shader = new LinearGradient(0,
        //       originalImage.getHeight(), 0, bitmapWithReflection.getHeight()
        //     + reflectionGap, 0x70ffffff, 0x00ffffff, Shader.TileMode.CLAMP);
        // Set the paint to use this shader (linear gradient)
        //   paint.setShader(shader);
        // Set the Transfer mode to be porter duff and destination in

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        canvas.drawRect(0, height, width, height, paint);
        return bitmapWithReflection;
    }
    public Bitmap getBothJoiningHead(Bitmap originalImage, int reflectionGap) {
        int width1 = originalImage.getWidth();
        int height1 = originalImage.getHeight();
        Log.v("widthEarlier", ""+width1+ " heightEarlier: " + height1);

        // This will not scale but will flip on the Y axis
        Matrix matrix = new Matrix();
        matrix.preScale(1.0f, -1.0f);

        // Create a Bitmap with the flip matix applied to it.
        // We only want the bottom half of the image
        Bitmap reflectionImage = Bitmap.createBitmap(originalImage, 0,
                height1 / 2, width1, height1 / 2, null, false);

        //this image will be shown on upper side of the mirror
        Bitmap halfImage = Bitmap.createBitmap(originalImage, 0,
                height1 / 2, width1, height1 / 2, matrix, false);

        int width = halfImage.getWidth();
        int height = halfImage.getHeight();

        // Create a new bitmap with same width but taller to fit reflection

        Bitmap bitmapWithReflection = Bitmap.createBitmap(width1,
                (height1), Bitmap.Config.ARGB_8888);
        // Create a new Canvas with the bitmap that's big enough for
        // the image plus gap plus reflection
        Log.v("widthLater", ""+bitmapWithReflection.getWidth()+ " heightLater: " + bitmapWithReflection.getHeight());
        Canvas canvas = new Canvas(bitmapWithReflection);
        // Draw in the original image
        canvas.drawBitmap(halfImage, 0, 0, null);
        // Draw in the gap
        Paint deafaultPaint = new Paint();
        // deafaultPaint.setColor(0xffffffff);

        canvas.drawRect(0, height, width, height + reflectionGap, deafaultPaint);

        canvas.drawBitmap(reflectionImage, 0, height + reflectionGap, null);
        Paint paint = new Paint();

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        canvas.drawRect(0, height, width, height, paint);
        return bitmapWithReflection;
    }
    public Bitmap getVerticalReflectionBothFlipped(Bitmap originalImage) {
        Bitmap bitmap;
        int width1 = originalImage.getWidth();
        int height1 = originalImage.getHeight();
        Log.v("widthEarlier", ""+width1+ " heightEarlier: " + height1);

        // This will not scale but will flip on the Y axis
        Matrix matrix = new Matrix();
        matrix.preScale(-1.0f, 1.0f);

        // Create a Bitmap with the flip matix applied to it.
        // We only want the bottom half of the image
        Bitmap reflectionImage = Bitmap.createBitmap(originalImage, 0,
                0, width1 / 2, height1, matrix, false);


        //this image will be shown on upper side of the mirror
        Bitmap halfImage = Bitmap.createBitmap(originalImage, 0,
                0, width1 / 2, height1, matrix, false);

        int width = halfImage.getWidth() * 2;
        int height = halfImage.getHeight();
        bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        Log.v("widthLater", ""+bitmap.getWidth()+ " heightLater: " + bitmap.getHeight());
        Canvas comboImage = new Canvas(bitmap);

        comboImage.drawBitmap(halfImage, 0f, 0f, null);
        comboImage.drawBitmap(reflectionImage, halfImage.getWidth(), 0f, null);
        return bitmap;
    }
    public Bitmap getVerticalReflectionOneDownward(Bitmap originalImage) {
        Bitmap bitmap;
        if (originalImage != null) {
            int width1 = originalImage.getWidth();
            int height1 = originalImage.getHeight();
            Log.v("widthEarlier", ""+width1+ " heightEarlier: " + height1);

            // This will not scale but will flip on the Y axis
            Matrix matrix = new Matrix();
            matrix.preScale(1.0f, -1.0f);

            // Create a Bitmap with the flip matix applied to it.
            // We only want the bottom half of the image
            Bitmap reflectionImage = Bitmap.createBitmap(originalImage, 0,
                    0, width1 / 2, height1, matrix, false);


            //this image will be shown on upper side of the mirror
            Bitmap halfImage = Bitmap.createBitmap(originalImage, 0,
                    0, width1 / 2, height1, null, false);

            int width = halfImage.getWidth() * 2;
            int height = halfImage.getHeight();
            bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            Log.v("widthLater", ""+bitmap.getWidth()+ " heightLater: " + bitmap.getHeight());
            Canvas comboImage = new Canvas(bitmap);

            comboImage.drawBitmap(halfImage, 0f, 0f, null);
            comboImage.drawBitmap(reflectionImage, halfImage.getWidth(), 0f, null);
            return bitmap;
        }
        return null;
    }

    public Bitmap getHalfUpAndHalfFlipped(Bitmap originalImage, int reflectionGap) {
        int width1 = originalImage.getWidth();
        int height1 = originalImage.getHeight();
        Log.v("widthEarlier", ""+width1+ " heightEarlier: " + height1);
        // This will not scale but will flip on the Y axis
        Matrix matrix = new Matrix();
        matrix.preScale(-1.0f, 1.0f);

        // Create a Bitmap with the flip matix applied to it.
        // We only want the bottom half of the image
        Bitmap reflectionImage = Bitmap.createBitmap(originalImage, 0,
                height1 / 2, width1, height1 / 2, matrix, false);

        //this image will be shown on upper side of the mirror
        Bitmap halfImage = Bitmap.createBitmap(originalImage, 0,
                height1 / 2, width1, height1 / 2, null, false);

        int width = halfImage.getWidth();
        int height = halfImage.getHeight();

        // Create a new bitmap with same width but taller to fit reflection

        Bitmap bitmapWithReflection = Bitmap.createBitmap(width1,
                (height1), Bitmap.Config.ARGB_8888);
        // Create a new Canvas with the bitmap that's big enough for
        // the image plus gap plus reflection
        Log.v("widthLater", ""+bitmapWithReflection.getWidth()+ " heightLater: " + bitmapWithReflection.getHeight());
        Canvas canvas = new Canvas(bitmapWithReflection);
        // Draw in the original image
        canvas.drawBitmap(halfImage, 0, 0, null);
        // Draw in the gap
        Paint deafaultPaint = new Paint();
        // deafaultPaint.setColor(0xffffffff);

        canvas.drawRect(0, height, width, height + reflectionGap, deafaultPaint);

        canvas.drawBitmap(reflectionImage, 0, height + reflectionGap, null);
        Paint paint = new Paint();

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        canvas.drawRect(0, height, width, height, paint);
        return bitmapWithReflection;
    }

    public Bitmap flipOperation(int degrees, Bitmap bitmap){
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);
        return Bitmap.createBitmap(bitmap, 0,0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    public Bitmap mirrorOperation(int pos, Bitmap bit){
        if (pos < mirrors.length){
            if (pos == 0){
                return getVerticalReflectionFullImage(bit);
            }else if (pos == 1){
                return getVerticalReflection(bit);
            }else if (pos == 2){
                return getHalfUpAndDownMirror(bit, 0);
            }else if (pos == 3){
                return getBothJoiningHead(bit, 0);
            }else if (pos == 4){
                return getVerticalReflectionBothFlipped(bit);
            }else if(pos == 5){
                return getVerticalReflectionOneDownward(bit);
            }else {
                return getHalfUpAndHalfFlipped(bit, 0);
            }
        }
        return null;
    }
    private File file;
    public void startSave(Bitmap bitmap, boolean isErase){
        file = new File(Environment.getExternalStorageDirectory(), Constants.fileName);
        if (file.exists()){
            file.delete();
        }
        try {
            FileOutputStream str = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, str);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Uri uri = fileProviderUri(file);
        Log.v("filePath", file.getAbsolutePath());
        if (isErase){
            CutOut.activity().src(uri).noCrop().start(this);
        }else {
            startActivityForResult(new Intent(this, MyTestingBlur.class).putExtra("save", file.getAbsolutePath()), blurCode);
        }
    }
    private AdSize getAdSize() {
        // Step 2 - Determine the screen width (less decorations) to use for the ad width.
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);

        // Step 3 - Get adaptive ad size and return for setting on the ad view.
        return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth);
    }

    public Uri fileProviderUri(File f){
        return FileProvider.getUriForFile(this, getPackageName(), f);
    }

    public ArrayList<File> getCategoryEffects(int position){
        String dir = dirName(position);
        ArrayList<File> listFiles = new ArrayList<>();
        if (!dir.equals("")){
            File file = new File(dirFiles().getAbsolutePath(), "/"+dir);
            File[] ff = file.listFiles();
            listFiles.addAll(Arrays.asList(ff));
        }else {
            return null;
        }
        return listFiles;
    }
    public String dirName(int position){
        if (position == 0){
            return "galaxy";
        }else if (position == 1){
            return "seasons";
        }else if (position == 2){
            return "clouds";
        }else if (position == 3){
            return "colors";
        }else if (position == 4){
            return "trees";
        }else {
            return "";
        }
    }
    private File dirFiles(){
        File file = new File(this.getExternalFilesDir(Constants.dirEffects).getAbsolutePath());
        if (!file.exists()){
            file.mkdirs();
        }
        return file;
    }
    public Point croppingRatio(int pos){
        Point p = new Point();
        switch (pos){
            case 0:{
                return null;
            }
            case 1:
            case 2: {
                p.x = 1;
                p.y = 1;
                return p;
            }
            case 3:{
                p.x = 2;
                p.y = 3;
                return p;
            }
            case 4:{
                p.x = 3;
                p.y = 2;
                return p;
            }
            case 5:{
                p.x = 3;
                p.y = 4;
                return p;
            }
        }
        return null;
    }

    public void hideLayout(){
        recyclerFramesAndEffects.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
        recyclerBack.setVisibility(View.GONE);
        isInRecycler = false;
        curTool = CURRENT_TOOL.DEF;
    }

    public void notify(int previous, int current){
        adapterResources.notifyItemChanged(previous);
        adapterResources.selectedPosition = current;
        adapterResources.notifyItemChanged(current);
    }

    public void setAdapter(int[] resourceList){
        if (isInCrop){
            disAbleCrop();
        }
        isInRecycler = true;
        recyclerView.setVisibility(View.VISIBLE);
        recyclerFramesAndEffects.setVisibility(View.GONE);
        recyclerBack.setVisibility(View.VISIBLE);
        adapterResources = new AdapterResources(this, resourceList, this);
        recyclerView.setAdapter(adapterResources);
    }

    public enum CURRENT_TOOL {
        EFFECTS,
        FRAMES,
        MIRRORS,
        ERASE,
        FLIP,
        CROP,
        BLUR,
        DEF,
        STICKER,
        TEXT_STICKER
    }
    public void disAbleCrop(){
        isInCrop = false;
        cropImageView.setVisibility(View.GONE);
        previewImage.setVisibility(View.VISIBLE);
    }
    public void setCurrentBitmap(Bitmap bitmap){
        if (previewImage.getVisibility() == View.GONE) {
            previewImage.setVisibility(View.VISIBLE);
        }
        previewImage.setImageBitmap(bitmap);
    }
    public void setEffect(Bitmap bitmap){
        effectImageView.setVisibility(View.VISIBLE);
        effectImageView.setImageBitmap(bitmap);
    }
    public void setFrame(Bitmap bitmap){
        frameImageView.setVisibility(View.VISIBLE);
        frameImageView.setImageBitmap(bitmap);
    }
    public void saveFinalImage(View v){
        v.setDrawingCacheEnabled(true);
//        return Bitmap.createBitmap(v.getDrawingCache());
        Bitmap bitmap = Bitmap.createBitmap(v.getDrawingCache());
        save(bitmap, true);
//        Bitmap b = Bitmap.createBitmap(v.getWidth() , v.getHeight(), Bitmap.Config.ARGB_8888);
//        Canvas c = new Canvas(b);
//        v.layout(0, 0, v.getLayoutParams().width, v.getLayoutParams().height);
//        v.draw(c);
//        return b;
    }

    public void saveFinalImage(Bitmap cam, Bitmap effectBitmap, Bitmap frameBitmap){
        Bitmap canvasBitmap = Bitmap.createBitmap(cam.getWidth(), cam.getHeight(), Bitmap.Config.ARGB_4444);
        Canvas canvas = new Canvas(canvasBitmap);
        Bitmap e;
        Bitmap f;
        canvas.drawBitmap(cam, 0,0, new Paint());
        if (effectBitmap != null && frameBitmap != null){
            e = Bitmap.createScaledBitmap(effectBitmap, cam.getWidth(), cam.getHeight(), false);
            f = Bitmap.createScaledBitmap(frameBitmap, cam.getWidth(), cam.getHeight(), false);
            canvas.drawBitmap(e, 0,0, new Paint());
            canvas.drawBitmap(f, 0,0, new Paint());
        } else if (frameBitmap != null){
            f = Bitmap.createScaledBitmap(frameBitmap, cam.getWidth(), cam.getHeight(), false);
            canvas.drawBitmap(f, 0,0, new Paint());
        }else if (effectBitmap != null){
            e = Bitmap.createScaledBitmap(effectBitmap, cam.getWidth(), cam.getHeight(), false);
            canvas.drawBitmap(e, 0,0, new Paint());
        }
        save(canvasBitmap, false);
    }

    private void save(Bitmap canvasBitmap, boolean formatPNG) {
        if (SharedPrefs.getInstance(this).getBooleanPref(Constants.directSave)){
            File f = new File(getExternalFilesDir(Constants.dirGallery), Constants.dirImages);
            if (!f.exists()){
                f.mkdirs();
            }
            try {
                String stamp = System.currentTimeMillis() + ".jpg";
                File file = new File(f, stamp);
                FileOutputStream str = new FileOutputStream(file);
                if (formatPNG){
                    canvasBitmap.compress(Bitmap.CompressFormat.PNG, 100, str);
                }else {
                    canvasBitmap.compress(Bitmap.CompressFormat.JPEG, 100, str);
                }
                str.close();
                str.flush();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return;
        }
        File file = new File(this.getExternalFilesDir(null).getAbsolutePath(),"/Testing");
        file.mkdirs();
        File f = new File(file, "myTesting.png");
        try {
            FileOutputStream str = new FileOutputStream(f);
            if (formatPNG){
                canvasBitmap.compress(Bitmap.CompressFormat.PNG, 100, str);
            }else {
                canvasBitmap.compress(Bitmap.CompressFormat.JPEG, 100, str);
            }
            str.close();
            str.flush();
            Intent intent = new Intent(this, PreviewActivity.class).putExtra("file", f.getAbsolutePath());
            if (formatPNG){
                intent.putExtra("from", "edit");
            }else {
                intent.putExtra("from", "camera");
            }
            startActivity(intent);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String save(String path, boolean formatPng){
        File f = new File(getExternalFilesDir(Constants.dirGallery), Constants.dirImages);
        if (!f.exists()) {
            f.mkdirs();
        }
        String stamp = System.currentTimeMillis() + ".jpg";
        File file = new File(f, stamp);
        try {
            Bitmap bitmap = BitmapFactory.decodeFile(path);
            FileOutputStream str = new  FileOutputStream(file);
            if (formatPng){
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, str);
            }else {
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, str);
            }
            str.close();
            str.flush();
            Toast.makeText(this, "Picture Saved", Toast.LENGTH_LONG).show();
            return file.getAbsolutePath();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void popUpMenu(View view){
        PopupMenu menu = new PopupMenu(this, view);
        menu.inflate(R.menu.timer_menu);
        menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.off:{
                        isTimerEnabled = false;
                        break;
                    }
                    case R.id.three:{
                        isTimerEnabled = true;
                        time = 3;
                        break;
                    }
                    case R.id.five:{
                        isTimerEnabled = true;
                        time = 5;
                        break;
                    }
                    case R.id.ten:{
                        isTimerEnabled = true;
                        time = 10;
                        break;
                    }
                }
                return false;
            }
        });
        menu.show();
    }

    public void save(PictureResult result){
//        PreviewActivity.result = result;
//        startActivity(new Intent(this, PreviewActivity.class));
    }

    public void transaction(Fragment fragment, int layout){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(layout, fragment).commit();
    }
    public void move(String flag){
        startActivity(new Intent(this, CategoriesActivity.class).putExtra("flag", flag));
    }
    public void move(Intent intent){
        startActivity(intent);
    }
    public String getPath(Context context, Uri uri ) {
        String result = null;
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = context.getContentResolver( ).query( uri, proj, null, null, null );
        if(cursor != null){
            if ( cursor.moveToFirst( ) ) {
                int column_index = cursor.getColumnIndexOrThrow( proj[0] );
                result = cursor.getString( column_index );
            }
            cursor.close( );
        }
        if(result == null) {
            result = "Not found";
        }
        return result;
    }
    public Uri getUri(String uri){
        File file = new File((uri));
        return (FileProvider.getUriForFile(this, "com.example.camera", file));
    }
    public String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }
    public FrameLayout adContainer;
    public void loadAd(){
        try {
            adContainer = findViewById(R.id.banner_ad);
            AdView adView = new AdView(this);
//        adView.setAdSize(AdSize.BANNER);
            adView.setAdSize(getAdSize());
            adView.setAdUnitId(Constants.bannerAd);
            AdRequest request = new AdRequest.Builder().build();
            adView.loadAd(request);
            adContainer.addView(adView);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void loadNativeAd(){
        try {
            adContainer = findViewById(R.id.adView);
            NativeAds.Companion.loadAdmobNativeAd(this, adContainer, Constants.nativeAd);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void adImage(){
        adContainer = findViewById(R.id.adView);
        ImageView imageView = new ImageView(this);
        imageView.setImageResource(R.drawable.ad);
        adContainer.addView(imageView);
        if (isNetworkAvailable()){
            loadNativeAd();
            loadAd();
        }
    }

    public DrawableSticker fileToSticker(String path){
        Bitmap bitmap = BitmapFactory.decodeFile(new File(path).getAbsolutePath());
        Drawable d = new BitmapDrawable(getResources(), bitmap);
        return new DrawableSticker(d);
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    public static Bitmap stack(Bitmap sentBitmap, int radius, boolean canReuseInBitmap) {
        // Stack Blur Algorithm by Mario Klingemann <mario@quasimondo.com>
        Bitmap bitmap;
        if (canReuseInBitmap) {
            bitmap = sentBitmap;
        } else {
            bitmap = sentBitmap.copy(sentBitmap.getConfig(), true);
        }

        if (radius < 1) {
            return (null);
        }

        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        int[] pix = new int[w * h];
        bitmap.getPixels(pix, 0, w, 0, 0, w, h);

        int wm = w - 1;
        int hm = h - 1;
        int wh = w * h;
        int div = radius + radius + 1;

        int r[] = new int[wh];
        int g[] = new int[wh];
        int b[] = new int[wh];
        int rsum, gsum, bsum, x, y, i, p, yp, yi, yw;
        int vmin[] = new int[Math.max(w, h)];

        int divsum = (div + 1) >> 1;
        divsum *= divsum;
        int dv[] = new int[256 * divsum];
        for (i = 0; i < 256 * divsum; i++) {
            dv[i] = (i / divsum);
        }

        yw = yi = 0;

        int[][] stack = new int[div][3];
        int stackpointer;
        int stackstart;
        int[] sir;
        int rbs;
        int r1 = radius + 1;
        int routsum, goutsum, boutsum;
        int rinsum, ginsum, binsum;

        for (y = 0; y < h; y++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            for (i = -radius; i <= radius; i++) {
                p = pix[yi + Math.min(wm, Math.max(i, 0))];
                sir = stack[i + radius];
                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);
                rbs = r1 - Math.abs(i);
                rsum += sir[0] * rbs;
                gsum += sir[1] * rbs;
                bsum += sir[2] * rbs;
                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }
            }
            stackpointer = radius;

            for (x = 0; x < w; x++) {

                r[yi] = dv[rsum];
                g[yi] = dv[gsum];
                b[yi] = dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (y == 0) {
                    vmin[x] = Math.min(x + radius + 1, wm);
                }
                p = pix[yw + vmin[x]];

                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[(stackpointer) % div];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi++;
            }
            yw += w;
        }
        for (x = 0; x < w; x++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            yp = -radius * w;
            for (i = -radius; i <= radius; i++) {
                yi = Math.max(0, yp) + x;

                sir = stack[i + radius];

                sir[0] = r[yi];
                sir[1] = g[yi];
                sir[2] = b[yi];

                rbs = r1 - Math.abs(i);

                rsum += r[yi] * rbs;
                gsum += g[yi] * rbs;
                bsum += b[yi] * rbs;

                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }

                if (i < hm) {
                    yp += w;
                }
            }
            yi = x;
            stackpointer = radius;
            for (y = 0; y < h; y++) {
                // Preserve alpha channel: ( 0xff000000 & pix[yi] )
                pix[yi] = (0xff000000 & pix[yi]) | (dv[rsum] << 16) | (dv[gsum] << 8) | dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (x == 0) {
                    vmin[y] = Math.min(y + r1, hm) * w;
                }
                p = x + vmin[y];

                sir[0] = r[p];
                sir[1] = g[p];
                sir[2] = b[p];

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[stackpointer];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi += w;
            }
        }

        bitmap.setPixels(pix, 0, w, 0, 0, w, h);

        return (bitmap);
    }
}

