package com.example.camera.Model;

public class GalleryModel {
    public String ImageUri;

    public GalleryModel(String imageUri) {
        this.ImageUri = imageUri;
    }

    public String getImageUri() {
        return ImageUri;
    }

    public void setImageUri(String imageUri) {
        ImageUri = imageUri;
    }


}
