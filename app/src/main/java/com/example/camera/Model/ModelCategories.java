package com.example.camera.Model;

import java.util.ArrayList;

public class ModelCategories {
    private int image;
    private String title;
    private ArrayList<ModelImage> list;


    public ModelCategories(int image, String title, ArrayList<ModelImage> list) {
        this.image = image;
        this.title = title;
        this.list = list;
    }

    public int getImage() {
        return image;
    }


    public String getTitle() {
        return title;
    }


    public ArrayList<ModelImage> getList() {
        return list;
    }

}
