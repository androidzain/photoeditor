package com.example.camera.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class ModelImage implements Parcelable{
    String url;
    String name;
    String parent;

    public ModelImage(String url, String parent, String name) {
        this.url = url;
        this.name = name;
        this.parent = parent;
    }

    protected ModelImage(Parcel in) {
        url = in.readString();
        name = in.readString();
        parent = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(url);
        dest.writeString(name);
        dest.writeString(parent);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ModelImage> CREATOR = new Creator<ModelImage>() {
        @Override
        public ModelImage createFromParcel(Parcel in) {
            return new ModelImage(in);
        }

        @Override
        public ModelImage[] newArray(int size) {
            return new ModelImage[size];
        }
    };

    public String getUrl() {
        return url;
    }

    public String getName() {
        return name;
    }

    public String getParent() {
        return parent;
    }
}
