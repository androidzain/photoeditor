package com.example.camera.Adaptors;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.camera.R;
import com.example.camera.listeners.RecyclerItemClickListener;
import com.example.camera.utils.MyHolder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class AdapterResources extends RecyclerView.Adapter<MyHolder> {
    private Context context;
    private int[] resources;
    private RecyclerItemClickListener.RecyclerListener listener;
    private int pre = 0;
    public int selectedPosition = -1;

    public AdapterResources(Context context, int[] resources, RecyclerItemClickListener.RecyclerListener listener) {
        this.context = context;
        this.resources = resources;
        this.listener = listener;
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyHolder(LayoutInflater.from(context).inflate(R.layout.recycler_frames, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {
        holder.imageView.setImageResource(resources[position]);
        if (selectedPosition == position){

        }else {

        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemResourcesClick(pre, holder.getAdapterPosition());
                pre = holder.getAdapterPosition();
            }
        });
    }

    @Override
    public int getItemCount() {
        return resources.length;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

}
