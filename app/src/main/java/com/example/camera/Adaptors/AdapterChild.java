package com.example.camera.Adaptors;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.example.camera.Model.ModelImage;
import com.example.camera.R;
import com.example.camera.listeners.RecyclerItemClickListener;
import com.example.camera.utils.MyHolder;

import java.io.File;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class AdapterChild extends RecyclerView.Adapter<MyHolder> {

    private Context context;
    private ArrayList<File> list;
    private RecyclerItemClickListener.RecyclerItemChild listener;

    public AdapterChild(Context context, ArrayList<File> list, RecyclerItemClickListener.RecyclerItemChild listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recycler_frames, parent, false);
        return new MyHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {
        Glide.with(context).load(list.get(position).getAbsolutePath()).override(150, 200).into(holder.imageView);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onChildClick(list.get(holder.getAdapterPosition()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
