package com.example.camera.Adaptors;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.example.camera.Model.ModelCategories;
import com.example.camera.R;
import com.example.camera.listeners.RecyclerItemClickListener;
import com.example.camera.models.CategoryModel;
import com.example.camera.models.CategoryModelForCameraScreen;
import com.example.camera.utils.MyHolder;

import java.io.File;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class CategoryModelAdapter extends RecyclerView.Adapter<MyHolder> {

    private Context context;
    private ArrayList<CategoryModelForCameraScreen> list;
    private RecyclerItemClickListener listener;
    private RecyclerItemClickListener.RecyclerItemChild childListener;
    private int prePos = -1;
    private boolean isSti;
    public CategoryModelAdapter(Context context, ArrayList<CategoryModelForCameraScreen> list, RecyclerItemClickListener listener,
                                RecyclerItemClickListener.RecyclerItemChild childListener, boolean isSti) {
        this.context = context;
        this.list = list;
        this.listener = listener;
        this.childListener = childListener;
        this.isSti = isSti;
    }

    public void setPrePos(int a){
        prePos = a;
        Log.v("prePositionAfter", String.valueOf(prePos));
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recycler_frames, parent, false);
        return new MyHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {
        if (position == 0){
            if (!isSti) {
                holder.imageView.setImageResource(R.drawable.no_effect);
                holder.itemView.setOnClickListener((v->{
                    listener.onItemClick(prePos, 0);
                }));
                return;
            }
        }
        CategoryModelForCameraScreen item = list.get(position);
        holder.imageView.setImageResource(item.getId());
        if (item.isSelected()) {
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, RecyclerView.HORIZONTAL, false);
            holder.recyclerView.setLayoutManager(layoutManager);
            holder.recyclerView.setAdapter(new AdapterChild(context, item.getListChild(), childListener));
            holder.recyclerView.setVisibility(View.VISIBLE);
            holder.selected.setVisibility(View.VISIBLE);
        } else {
            holder.recyclerView.setVisibility(View.GONE);
            holder.selected.setVisibility(View.GONE);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.v("prePos", String.valueOf(prePos));
                listener.onItemClick(prePos, holder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

}
