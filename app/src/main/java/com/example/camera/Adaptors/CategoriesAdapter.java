package com.example.camera.Adaptors;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.camera.Model.ModelCategories;
import com.example.camera.R;
import com.example.camera.listeners.RecyclerItemClickListener;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.CategoryHolder> {

    private ArrayList<ModelCategories> list;
    private RecyclerItemClickListener listener;
    public CategoriesAdapter(ArrayList<ModelCategories> list, RecyclerItemClickListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public CategoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_items_categories, parent, false);
        return new CategoryHolder(view);
    }

    int prePos = -1;
    @Override
    public void onBindViewHolder(@NonNull CategoryHolder holder, int position) {
        ModelCategories item = list.get(position);
        holder.imageView.setImageResource(item.getImage());
        holder.itemView.setOnClickListener(view -> {
            listener.onItemClick(prePos, holder.getAdapterPosition());
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    class CategoryHolder extends RecyclerView.ViewHolder{
        ImageView imageView;
        CategoryHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.image_view);
        }
    }
}
