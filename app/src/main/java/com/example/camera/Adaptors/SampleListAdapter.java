package com.example.camera.Adaptors;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.camera.Model.ModelImage;
import com.example.camera.R;
import com.example.camera.listeners.RecyclerItemClickListener;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

public class SampleListAdapter extends RecyclerView.Adapter<SampleListAdapter.SampleHolder> {
    private ArrayList<ModelImage> list;
    private RecyclerItemClickListener listener;
    private Context context;

    public SampleListAdapter(Context context, ArrayList<ModelImage> list, RecyclerItemClickListener listener) {
        this.list = list;
        this.listener = listener;
        this.context = context;
    }
    private int prePos = -1;

    @NonNull
    @Override
    public SampleHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_samples_list, parent, false);
        return new SampleHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SampleHolder holder, int position) {
        //https://docs.google.com/uc?id=18F2oNjVUmyX7Kez5r7A6tOlrN0u5oZOr
        Glide.with(context).load(list.get(position).getUrl()).override(150, 200).listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                holder.progressBar.setVisibility(View.GONE);
                return false;
            }
        }).into(holder.image);
        holder.itemView.setOnClickListener(view ->{
            listener.onItemClick(prePos, holder.getAdapterPosition());
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    class SampleHolder extends RecyclerView.ViewHolder{
        ImageView image;
        ProgressBar progressBar;
        public SampleHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image_view);
            progressBar = itemView.findViewById(R.id.progress_bar);
        }
    }
}
