package com.example.camera.Adaptors;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.camera.Model.GalleryModel;
import com.example.camera.R;

import java.util.ArrayList;

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.ViewHolder> {
    private ArrayList<GalleryModel> mData = new ArrayList<>();
    private Context context;
    //    private ItemClickListener mClickListener;


    private static final String TAG = GalleryAdapter.class.getSimpleName();
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;


    // data is passed into the constructor
    public GalleryAdapter(Context context, ArrayList<GalleryModel> data) {
        this.context = context;
        this.mData = data;
    }


    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.recycler_gallery, parent, false);


        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

//        Glide.get()
//                .load(mData.get(position).getComicImageUrl())
//                .into(holder.comicImg);
        Uri uri = Uri.parse(mData.get(position).getImageUri());
        holder.galleryImage.setImageURI(uri);
        //  Glide.with(context).load(uri).into(holder.galleryImage);
        Log.i("URI in recycler", uri.toString());
        holder.galleryImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                String currentImage = mData.get(position).getImageUri();
//                Intent intent = new Intent(context, Share.class);
//                intent.putExtra("WorkImage", currentImage);
//                Log.i("URI in recycler intent", currentImage);
//                context.startActivity(intent);
//                ((Activity)context).finish();
//                Utils.isMyWork = true;
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mData == null) {
            return 0;
        }
        return mData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView galleryImage;

        ViewHolder(View itemView) {
            super(itemView);
            galleryImage = itemView.findViewById(R.id.galleryImage);
        }
    }
}