package com.example.camera.services;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.widget.Toast;

import com.example.camera.Model.ModelImage;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import androidx.annotation.Nullable;

import static com.example.camera.utils.Constants.curDir;

public class DownloadingService extends IntentService {
    private int UPDATE_PROGRESS = 909;
    public DownloadingService() {
        super("MyService");
    }

    String dir;
    String subDir;
    String fileName;
    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        ModelImage item = intent.getParcelableExtra("item");
        String urlToDownload = item.getUrl();
        fileName = item.getName();
        dir = intent.getStringExtra("dir");
        subDir = item.getParent();
        ResultReceiver receiver = (ResultReceiver) intent.getParcelableExtra("receiver");
        try {

            //create url and connect
            URL url = new URL(urlToDownload);
            URLConnection connection = url.openConnection();
            connection.connect();

            // this will be useful so that you can show a typical 0-100% progress bar
            int fileLength = connection.getContentLength();

            // download the file
            InputStream input = new BufferedInputStream(connection.getInputStream());
            File file;
            if (dir != null){
                file = getExternalFilesDir(dir);
            }else {
                file = getExternalFilesDir(curDir);
            }
//            File dir = new File(file, new Utils().getDir(this, subDir).getAbsolutePath());
            if (subDir == null) {
                Toast.makeText(this, "Null subDir", Toast.LENGTH_SHORT).show();
                return;
            }
            File dir = new File(file, subDir);
            if (!dir.exists()){
                dir.mkdirs();
            }
            String path = dir.getAbsolutePath() + "/" + fileName;
            OutputStream output = new FileOutputStream(path);

            byte data[] = new byte[1024];
            long total = 0;
            int count;
            while ((count = input.read(data)) != -1) {
                total += count;

                // publishing the progress....
                Bundle resultData = new Bundle();
                resultData.putInt("progress" ,(int) (total * 100 / fileLength));
                receiver.send(UPDATE_PROGRESS, resultData);
                output.write(data, 0, count);
            }

            // close streams
            output.flush();
            output.close();
            input.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        Bundle resultData = new Bundle();
        resultData.putInt("progress" ,100);

        receiver.send(UPDATE_PROGRESS, resultData);
    }
}
