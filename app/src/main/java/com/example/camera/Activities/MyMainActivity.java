package com.example.camera.Activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.net.MailTo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.camera.BaseActivity;
import com.example.camera.R;
import com.example.camera.utils.Constants;
import com.example.camera.utils.SharedPrefs;
import com.github.gabrielbb.cutout.CutOut;
import com.google.android.gms.ads.AdView;
import com.google.android.material.navigation.NavigationView;
import com.otaliastudios.cameraview.controls.Preview;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

public class MyMainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {
    private ImageView edit, sticker, background, filters, camera, mFrames, mEffects, featureOne, featureTwo;
    private final int pickImage = 193;
    private ImageView drawer;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeResources();
        edit.setOnClickListener(this);
        sticker.setOnClickListener(this);
        background.setOnClickListener(this);
        filters.setOnClickListener(this);
        camera.setOnClickListener(this);
        featureOne.setOnClickListener(this);
        featureTwo.setOnClickListener(this);
        drawer.setOnClickListener(this);
        mFrames.setOnClickListener(this);
        mEffects.setOnClickListener(this);
        navigationView.setNavigationItemSelectedListener(this);
        SharedPrefs.getInstance(this).savePref(Constants.isClassicSaved, false);
        if (!SharedPrefs.getInstance(this).getBooleanPref(Constants.isClassicSaved)){
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
            PackageManager.PERMISSION_GRANTED){
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE}, 323);
            } else {
                saveEffect();
                saveFrame();
                SharedPrefs.getInstance(this).savePref(Constants.isClassicSaved , true);
            }
        }
        adImage();
    }

    private boolean isEdit = false;

    @Override
    public void initializeResources() {
        setContentView(R.layout.activity_my_main_layout);
        edit = findViewById(R.id.edit_photo);
        sticker = findViewById(R.id.stickers);
        background = findViewById(R.id.background_eraser);
        filters = findViewById(R.id.gallery);
        camera = findViewById(R.id.camera_btn);
        featureOne = findViewById(R.id.feature_one);
        featureTwo = findViewById(R.id.feature_two);
        drawer = findViewById(R.id.drawer);
        drawerLayout = findViewById(R.id.drawerLayout);
        navigationView = findViewById(R.id.navigation_view);
        mEffects = findViewById(R.id.m_effects);
        mFrames = findViewById(R.id.m_frames);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.edit_photo:{
                pick(true);
                break;
            }
            case R.id.stickers:{
                move("frames");
                break;
            }
            case R.id.drawer:{
                isInDrawer = true;
                drawerLayout.openDrawer(GravityCompat.START);
                break;
            }
            case R.id.background_eraser:{
                pick(false);
                break;
            }
            case R.id.gallery:{
                startActivity(new Intent(this, GalleryActivity.class).putExtra("fromHome", true));
                break;
            }
            case R.id.m_effects:{
                move("stickers");
                break;
            }
            case R.id.m_frames:{
                move("frames");
                break;
            }
            case R.id.camera_btn:{
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                    if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED ||
                            checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
                        move(new Intent(MyMainActivity.this, MyCameraActivity.class));
                    }
                    else {
                        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 505);
                    }
                    return;
                }
                move(new Intent(MyMainActivity.this, MyCameraActivity.class));
                break;
            }
            case R.id.feature_one:{
                move("effects");
                break;
            }
            case R.id.feature_two:{
                move("stickers");
                break;
            }
        }
    }

    private void pick(boolean a){
        Intent intent = new Intent(Intent.ACTION_PICK);
//                intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("image/*");
        startActivityForResult(intent, pickImage);
        isEdit = a;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == pickImage) {
                final Uri imageUri = data.getData();
                assert imageUri != null;
                String p = getRealPathFromURI(imageUri);
                Log.v("path", p);
                if (isEdit) {
                    move(new Intent(MyMainActivity.this, MyEditingActivity.class)
                            .putExtra("imageUri", String.valueOf(p)));
                    return;
                }
                Uri uri = fileProviderUri(new File(p));
                CutOut.activity().src(uri).noCrop().start(this);
            }
            if (requestCode == CutOut.CUTOUT_ACTIVITY_REQUEST_CODE) {
                Uri uri = CutOut.getUri(data);
                String a = save(getRealPathFromURI(uri), true);
//                startActivity(new Intent(this, PreviewActivityShare.class).putExtra("file", getRealPathFromURI(uri)));
                startActivity(new Intent(this, PreviewActivityShare.class).putExtra("file", a));
            }
        }
    }

    @Override
    public void onItemResourcesClick(int pre, int position) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 &&grantResults[0] == PackageManager.PERMISSION_GRANTED){
            if (requestCode == 505){
                move(new Intent(MyMainActivity.this, MyCameraActivity.class));
            }else if (requestCode == 323){
//                copyAssets();
                saveEffect();
                saveFrame();
                SharedPrefs.getInstance(this).savePref(Constants.isClassicSaved , true);
            }
        }
    }
    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while((read = in.read(buffer)) != -1){
            out.write(buffer, 0, read);
        }
    }
    private void saveEffect() {
        try {
            String[] list = getAssets().list("effects");
            File  f = null;
            File ff = getExternalFilesDir(Constants.dirEffects);
            if (!ff.exists()){
                if (ff.mkdirs()){
                    f = new File(ff, "Snow");
                    if (!f.exists()){
                        f.mkdirs();
                    }
                }
            }
            if (list != null) {
                if (list.length > 0) {
                    for (String fName : list) {
                        try {
                            InputStream in= null;
                            OutputStream out = null;
                            if (!fName.endsWith(".png")){
                                continue;
                            }
                            if (f == null){
                                f = new File(ff, "Snow");
                                if (!f.exists()){
                                    f.mkdirs();
                                }
                            }
                            in = getAssets().open("effects/"+fName);
                            File outFile = new File(f, fName);
                            out = new FileOutputStream(outFile);
                            copyFile(in, out);
                            Log.v("nameFile", fName);
                            try {
                                in.close();
                                out.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void saveFrame() {
        try {
            String[] list = getAssets().list("frames");
            File  f = null;
            File ff = getExternalFilesDir(Constants.dirFrames);
            if (!ff.exists()){
                if (ff.mkdirs()){
                    f = new File(ff, "Classic");
                    if (!f.exists()){
                        f.mkdirs();
                    }
                }
            }
            if (list != null) {
                if (list.length > 0) {
                    for (String fName : list) {
                        try {
                            InputStream in= null;
                            OutputStream out = null;
                            if (!fName.endsWith(".png")){
                                continue;
                            }
                            if (f == null){
                                f = new File(ff, "Classic");
                                if (!f.exists()){
                                    f.mkdirs();
                                }
                            }
                            in = getAssets().open("frames/"+fName);
                            File outFile = new File(f, fName);
                            out = new FileOutputStream(outFile);
                            copyFile(in, out);
                            Log.v("nameFile", fName);
                            try {
                                in.close();
                                out.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean isInDrawer = false;
    @Override
    public void onBackPressed() {
        if (isInDrawer){
            isInDrawer = false;
            drawerLayout.closeDrawer(GravityCompat.START);
            return;
        }
        super.onBackPressed();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()){
            case R.id.edit_photo:{
                pick(true);
                break;
            }
            case R.id.erase:{
                pick(false);
                break;
            }
            case R.id.stickers:{
                move("stickers");
                break;
            }
            case R.id.gallery:{
                startActivity(new Intent(this, GalleryActivity.class));
                break;
            }
            case R.id.rate:{
                //
                break;
            }
            case R.id.share:{
                //
                break;
            }
        }
        return false;
    }
}
