package com.example.camera.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.camera.Adaptors.GalleryAdapter;
import com.example.camera.BaseActivity;
import com.example.camera.BuildConfig;
import com.example.camera.Model.GalleryModel;
import com.example.camera.R;
import com.example.camera.Utils;
import com.example.camera.ads.InterstitialAds;
import com.example.camera.utils.Constants;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Collections;

public class GalleryActivity extends BaseActivity {
    static final String[] EXTENSIONS = new String[]{
            "jpg", "png", "bmp" // and other formats you need
    };
    Uri ImageUri;
    ArrayList<GalleryModel> arrayList = new ArrayList<>();
    RecyclerView recyclerView;
    TextView text;
    ImageView home;
    ImageView back;
    GalleryAdapter galleryAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        recyclerView = findViewById(R.id.recyclerView);
        home = findViewById(R.id.home);
        text = findViewById(R.id.nFound);
        back = findViewById(R.id.back);
        if (isNetworkAvailable()){
            loadAd();
        }
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
//        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
//                Environment.DIRECTORY_PICTURES), Utils.MyCameraApp);
        File mediaStorageDir = new File(getExternalFilesDir(Constants.dirGallery), Constants.dirImages);
        if (!mediaStorageDir.exists()){
            mediaStorageDir.mkdirs();
        }
        File dir = new File(String.valueOf(mediaStorageDir));
        File[] filelist = dir.listFiles(IMAGE_FILTER);
        if (filelist != null) {
            for (File f : filelist) {
            //    Log.i("All the gallery Items", "Image:" + f.toString());

//                Uri uri = FileProvider.getUriForFile(GalleryActivity.this, BuildConfig.APPLICATION_ID + ".provider", f);
                String a = String.valueOf(f.getAbsolutePath());
                arrayList.add(new GalleryModel(a));

              //  Log.i(" Gallery Items URIs", "Image:" + uri.toString());
            }
            Collections.reverse(arrayList);
            galleryAdapter = new GalleryAdapter(this, arrayList);
            recyclerView.setAdapter(galleryAdapter);
        }
        else {
            Utils.Toast(GalleryActivity.this,"You Don't Have Recent Work");
        }
        if (arrayList.size() == 0){
            text.setVisibility(View.VISIBLE);
        }
        home.setOnClickListener(view -> {
            if (getIntent() != null) {
                if (getIntent().getBooleanExtra("fromHome", false)) {
                    finish();
                } else {
                    setResult(RESULT_OK);
                    finish();
                }
            }else {
                setResult(RESULT_OK);
                finish();
            }
            if (InterstitialAds.Companion.isAdLoaded(GalleryActivity.this, Constants.interstitialAdId)) {
                InterstitialAds.Companion.showAd(GalleryActivity.this, Constants.interstitialAdId);
            }
        });
        back.setOnClickListener((v)->{
            finish();
        });
    }
    static final FilenameFilter IMAGE_FILTER = new FilenameFilter() {
        @Override
        public boolean accept(final File dir, final String name) {
            for (final String ext : EXTENSIONS) {
                if (name.endsWith("." + ext)) {
                    return (true);
                }
            }
            return (false);
        }
    };
    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void initializeResources() {

    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onItemResourcesClick(int pre, int position) {

    }

}
