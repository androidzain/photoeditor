package com.example.camera.Activities

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.example.camera.BaseActivity
import com.example.camera.R
import com.example.camera.utils.Constants
import com.otaliastudios.cameraview.PictureResult
import kotlinx.android.synthetic.main.layout_preview.*
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException

class PreviewActivity: BaseActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_preview)
        if (intent != null){
            if (intent.getStringExtra("file") != null){
                val bitmap = BitmapFactory.decodeFile(intent.getStringExtra("file"))
                image_view.setImageBitmap(bitmap)
            }
        }else{
            finish()
            return
        }
        loadAd()
        save.setOnClickListener {
//            startActivity(Intent(this, PreviewActivityShare::class.java).putExtra("file", intent.getStringExtra("file")).putExtra("fromCapture", "ccc"))
            var a = ""
            var b = false
            if (intent.getStringExtra("from") == "edit"){
                a = save(intent.getStringExtra("file"), true)
                b = true
            }else{
                a = save(intent.getStringExtra("file"), false)
            }
            startActivityForResult(Intent(this, PreviewActivityShare::class.java)
                    .putExtra("file", a).putExtra("fromEdit", b), 767)
        }
        back.setOnClickListener {
            finish()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK){
            if (requestCode == 767){
                finish()
            }
        }
    }

    //    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
//        menuInflater.inflate(R.menu.perview_menu, menu)
//        return true
//    }
//
//    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
//        if (item!!.itemId == R.id.save){
//            if(bitmap != null){
//                val f = File(getExternalFilesDir(Constants.dirGallery), Constants.dirImages)
//                if (!f.exists()) {
//                    f.mkdirs()
//                }
//                val stamp = System.currentTimeMillis().toString() + ".jpg";
//                val file = File(f, stamp);
//                try {
//                    val str = FileOutputStream(file)
//                    bitmap!!.compress(Bitmap.CompressFormat.JPEG, 100, str)
//                    str.close()
//                    str.flush()
//                    Toast.makeText(this, "Picture Saved", Toast.LENGTH_LONG).show()
//                } catch (e: FileNotFoundException) {
//                    e.printStackTrace()
//                } catch (e: IOException) {
//                    e.printStackTrace()
//                }
//
//            }
//        }
//        return false
//    }

    override fun onClick(p0: View?) {

    }

    override fun initializeResources() {

    }

    override fun onItemResourcesClick(pre: Int, position: Int) {

    }
}