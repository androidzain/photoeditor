package com.example.camera.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.camera.BuildConfig;
import com.example.camera.R;
import com.example.camera.utils.Constants;
import com.example.camera.utils.SharedPrefs;

public class SettingsActivity extends AppCompatActivity {
    private SwitchCompat startFromHomeBtn, frontCamBtn, mirrorEffectBtn, directSaveBtn, soundBtn;
    private ImageView settingsGalleryBtn;
    private TextView shareBtn, rateBtn, waterMarkBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        inIt();
        setToggleState();
        setListener();
    }

    @Override
    protected void onResume() {
        super.onResume();
        View decorView = getWindow().getDecorView();// hide status bar
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
    }

    private void inIt() {
        settingsGalleryBtn = findViewById(R.id.settingsBtn);
        shareBtn = findViewById(R.id.shareBtn);
        rateBtn = findViewById(R.id.rateBtn);
        startFromHomeBtn = findViewById(R.id.startFromHomeBtn);
        frontCamBtn = findViewById(R.id.frontCamBtn);
        mirrorEffectBtn = findViewById(R.id.mirrorEffectBtn);
        directSaveBtn = findViewById(R.id.directSaveBtn);
        soundBtn = findViewById(R.id.soundBtn);
        waterMarkBtn = findViewById(R.id.waterMarkBtn);
    }

    //            Boolean b = Utils.getBooleanData(PermissionsActivity.this, SharedPreference.SETTINGS_PREFS, SharedPreference.START_FROM_HOME);
    private void setListener() {
        settingsGalleryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SettingsActivity.this, GalleryActivity.class);
                startActivity(intent);
            }
        });
        startFromHomeBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                SharedPrefs.getInstance(SettingsActivity.this).savePref(Constants.startFromHome, b);
            }
        });
        frontCamBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                SharedPrefs.getInstance(SettingsActivity.this).savePref(Constants.frontCamera, b);
            }
        });
        mirrorEffectBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                SharedPrefs.getInstance(SettingsActivity.this).savePref(Constants.mirrorEffect, b);
            }
        });
        directSaveBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                SharedPrefs.getInstance(SettingsActivity.this).savePref(Constants.directSave, b);
            }
        });
        soundBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                SharedPrefs.getInstance(SettingsActivity.this).savePref(Constants.sound, b);
            }
        });
        waterMarkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //
            }
        });

        shareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Photo Editor");
                    String shareMessage = "\nLet me recommend you this application\n\n";
                    shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + "\n\n";
                    shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                    startActivity(Intent.createChooser(shareIntent, "choose one"));
                } catch (Exception e) {
                    //e.toString();
                }

            }
        });
        rateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("market://details?id=" + getPackageName());
                Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
                try {
                    startActivity(myAppLinkToMarket);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(SettingsActivity.this, " unable to find market app", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void setToggleState() {
        startFromHomeBtn.setChecked(SharedPrefs.getInstance(this).getBooleanPref(Constants.startFromHome));
        frontCamBtn.setChecked(SharedPrefs.getInstance(this).getBooleanPref(Constants.frontCamera));directSaveBtn.setChecked(SharedPrefs.getInstance(this).getBooleanPref(Constants.directSave));
        mirrorEffectBtn.setChecked(SharedPrefs.getInstance(this).getBooleanPref(Constants.mirrorEffect));
        soundBtn.setChecked(SharedPrefs.getInstance(this).getBooleanPref(Constants.sound, true));

    }
}
