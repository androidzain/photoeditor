package com.example.camera.Activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.camera.BaseActivity;
import com.example.camera.Fragments.FragmentEffects;
import com.example.camera.Fragments.FragmentFrames;
import com.example.camera.Fragments.FragmentStickers;
import com.example.camera.R;
import com.example.camera.ads.InterstitialAds;
import com.example.camera.utils.Constants;
import com.google.android.material.tabs.TabLayout;

import androidx.annotation.Nullable;

public class CategoriesActivity extends BaseActivity {
    private TabLayout tabLayout;
    private enum  CURRENT_FRAGMENT{
        FRAME,
        EFFECT,
        STICKER
    }
    CURRENT_FRAGMENT current_fragment;
    private TextView title;
    private ImageView back;
    private ImageView home;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.categories_activity);
        tabLayout = findViewById(R.id.tab_layout);
        title = findViewById(R.id.title);
        home = findViewById(R.id.home);
        back = findViewById(R.id.back);
        loadAd();
        TabLayout.Tab tab;
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int position = tab.getPosition();
                if (position == 0){
                    if (current_fragment != CURRENT_FRAGMENT.FRAME){
                        current_fragment = CURRENT_FRAGMENT.FRAME;
                        transaction(new FragmentFrames(), R.id.frame_layout);
                        title.setText("Frames");
                    }
                }else if (position == 1){
                    if (current_fragment != CURRENT_FRAGMENT.EFFECT){
                        current_fragment = CURRENT_FRAGMENT.EFFECT;
                        transaction(new FragmentEffects(), R.id.frame_layout);
                        title.setText("Effects");
                    }
                }else {
                    if (current_fragment != CURRENT_FRAGMENT.STICKER){
                        current_fragment = CURRENT_FRAGMENT.STICKER;
                        transaction(new FragmentStickers(), R.id.frame_layout);
                        title.setText("Stickers");
                    }
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        if (getIntent().getStringExtra("flag") != null){
            if (getIntent().getStringExtra("flag").equals("stickers")){
                tab = tabLayout.getTabAt(2);
                tab.select();
                current_fragment = CURRENT_FRAGMENT.STICKER;
                transaction(new FragmentStickers(), R.id.frame_layout);
                title.setText("Stickers");
            }else if (getIntent().getStringExtra("flag").equals("frames")){
                tab = tabLayout.getTabAt(0);
                tab.select();
                current_fragment = CURRENT_FRAGMENT.FRAME;
                transaction(new FragmentFrames(), R.id.frame_layout);
                title.setText("Frames");
            }else {
                tab = tabLayout.getTabAt(1);
                tab.select();
                current_fragment = CURRENT_FRAGMENT.EFFECT;
                transaction(new FragmentEffects(), R.id.frame_layout);
                title.setText("Effects");
            }
        }else {
            tab = tabLayout.getTabAt(0);
            tab.select();
            current_fragment = CURRENT_FRAGMENT.FRAME;
        }
//        title.setText("Frames");
        back.setOnClickListener(v->{
            super.onBackPressed();
        });
        home.setOnClickListener(v->{
            if (InterstitialAds.Companion.isAdLoaded(this, Constants.interstitialAdId)){
                InterstitialAds.Companion.showAd(this, Constants.interstitialAdId);
            }
            finish();
        });
    }

    @Override
    public void initializeResources() {

    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onItemResourcesClick(int pre, int position) {

    }

}
