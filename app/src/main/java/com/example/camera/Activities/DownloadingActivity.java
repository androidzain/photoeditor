package com.example.camera.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.camera.Adaptors.SampleListAdapter;
import com.example.camera.BaseActivity;
import com.example.camera.Model.ModelImage;
import com.example.camera.R;
import com.example.camera.listeners.RecyclerItemClickListener;
import com.example.camera.services.DownloadingService;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.rewarded.RewardItem;
import com.google.android.gms.ads.rewarded.RewardedAd;
import com.google.android.gms.ads.rewarded.RewardedAdCallback;
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback;

import java.io.File;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class DownloadingActivity extends BaseActivity implements RecyclerItemClickListener {
    private ImageView sample;
    private ImageView back;
    private ProgressBar progressBar;
    private Button button;
    private ArrayList<ModelImage> listSamples;
    private SampleListAdapter adapter;
    private ProgressDialog progressDialog;
    private String dir;
    private RewardedAd rewardedAd;
    private TextView label;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeResources();
        if (isNetworkAvailable()){
            loadAd();
        }
        rewardedAd = new RewardedAd(this, getString(R.string.rewarded_ad_id));
        rewardedAd.loadAd(new AdRequest.Builder().build(), adLoadCallback);
        listSamples = (ArrayList<ModelImage>) getIntent().getSerializableExtra("samples_list");
        dir = getIntent().getStringExtra("dir");
        boolean a = false;
        File file = getExternalFilesDir(dir);
        if (file != null) {
            if (!file.exists()){
                file.mkdirs();
                a= true;
            }
        }
        Log.d("huijui", ""+listSamples.size());
        if (!a) {
            file = new File(file, listSamples.get(0).getParent());
            if (file.exists()){
                for (int i = 0; i<listSamples.size(); i++){
                    File f = new File(file, listSamples.get(i).getName());
                    if (f.exists()){
                        listSamples.remove(i);
                    }
                }
            }
        }
        if (listSamples.size() == 0){
            label.setVisibility(View.VISIBLE);
            button.setVisibility(View.GONE);
            return;
        }
        adapter = new SampleListAdapter(this, listSamples, this);
        recyclerView.setAdapter(adapter);
        Glide.with(this).load(Uri.parse(listSamples.get(0).getUrl())).listener(new RequestListener<Uri, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, Uri model, Target<GlideDrawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, Uri model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                progressBar.setVisibility(View.GONE);
                return false;
            }
        }).into(sample);
    }

    @Override
    public void initializeResources() {
        setContentView(R.layout.downloading_activity);
        recyclerView = findViewById(R.id.recyclerView);
        sample = findViewById(R.id.sample);
        back = findViewById(R.id.back_image);
        progressBar = findViewById(R.id.progress_bar);
        button = findViewById(R.id.download);
        label = findViewById(R.id.text);
        button.setOnClickListener(this);
        back.setOnClickListener(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.download){
            if (rewardedAd.isLoaded()){
                isUpdated = false;
                rewardedAd.show(this, adCallback);
            } else {
                rewardedAd.loadAd(new AdRequest.Builder().build(), adLoadCallback);
                Toast.makeText(this, "Could not load ad. Please try again later.", Toast.LENGTH_SHORT).show();
            }
        }
        else if (view.getId() == R.id.back_image){
            finish();
        }
    }

    private int currentPosition = 0;

    @Override
    public void onItemClick(int pre, int position) {
        currentPosition = position;
        progressBar.setVisibility(View.VISIBLE);
        Glide.with(this).load(Uri.parse(listSamples.get(position).getUrl())).listener(new RequestListener<Uri, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, Uri model, Target<GlideDrawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, Uri model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                progressBar.setVisibility(View.GONE);
                return false;
            }
        }).into(sample);
//        sample.setImageURI(Uri.parse(listSamples.get(position).getUrl()));
    }

    private boolean isUpdated = false;

    @Override
    public void onItemResourcesClick(int pre, int position) {

    }

    private class DownloadReceiver extends ResultReceiver{

        public DownloadReceiver(Handler handler) {
            super(handler);
            progressDialog = new ProgressDialog(DownloadingActivity.this);
            progressDialog.show();
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            super.onReceiveResult(resultCode, resultData);
            if (resultCode == 909){
                int progress = resultData.getInt("progress");
                progressDialog.setProgress(progress);
                if (progress == 100){
                    if (!isUpdated){
                        isUpdated = true;
                        listSamples.remove(currentPosition);
                        adapter.notifyDataSetChanged();
                        int a = currentPosition;
                        if (a < listSamples.size()){
                            Glide.with(DownloadingActivity.this).load(Uri.parse(listSamples.get(a).getUrl())).into(sample);
                            rewardedAd.loadAd(new AdRequest.Builder().build(), adLoadCallback);
                        }else if (a-1>=0 && a-1 < listSamples.size()){
                            Glide.with(DownloadingActivity.this).load(Uri.parse(listSamples.get(a-1).getUrl())).into(sample);
                            currentPosition = a-1;
                            rewardedAd.loadAd(new AdRequest.Builder().build(), adLoadCallback);
                        } else {
                            label.setVisibility(View.VISIBLE);
                            button.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.GONE);
                        }
                    }
                    Toast.makeText(DownloadingActivity.this, "Completed", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            }
        }
    }
    private RewardedAdLoadCallback adLoadCallback = new RewardedAdLoadCallback(){
        @Override
        public void onRewardedAdLoaded() {
            super.onRewardedAdLoaded();
        }

        @Override
        public void onRewardedAdFailedToLoad(int i) {
            super.onRewardedAdFailedToLoad(i);
        }
    };

    RewardedAdCallback adCallback = new RewardedAdCallback() {
        @Override
        public void onRewardedAdOpened() {
            // Ad opened.
        }

        @Override
        public void onRewardedAdClosed() {
            // Ad closed.
        }

        @Override
        public void onUserEarnedReward(@NonNull RewardItem reward) {
            Intent intent = new Intent(DownloadingActivity.this, DownloadingService.class);
            intent.putExtra("item", listSamples.get(currentPosition));
            intent.putExtra("dir", dir);
            intent.putExtra("receiver", new DownloadReceiver(new Handler()));
            startService(intent);
        }

        @Override
        public void onRewardedAdFailedToShow(int errorCode) {
            Toast.makeText(DownloadingActivity.this, "Ad Failed To Show. Please Try again later", Toast.LENGTH_SHORT).show();
        }
    };
}
