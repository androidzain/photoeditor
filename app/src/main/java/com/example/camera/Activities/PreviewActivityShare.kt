package com.example.camera.Activities

import android.app.Activity
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.View
import com.example.camera.BaseActivity
import kotlinx.android.synthetic.main.activity_my_editing.*
import kotlinx.android.synthetic.main.layout_preview_share.*
import android.content.Intent
import android.net.Uri
import android.widget.Toast
import android.content.pm.ResolveInfo
import android.content.ActivityNotFoundException
import android.graphics.Bitmap
import android.os.Environment
import android.provider.MediaStore
import com.example.camera.utils.Constants
import com.github.gabrielbb.cutout.CutOut
import kotlinx.android.synthetic.main.layout_drawer.*
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException


class PreviewActivityShare: BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initializeResources()
        val path = intent.getStringExtra("file")
        imageView.setImageBitmap(BitmapFactory.decodeFile(path))
        if (intent.getBooleanExtra("fromEdit", false)){
            eidt.visibility = View.GONE
        }
        backA.setOnClickListener {
            if (intent.getStringExtra("fromCapture") == null){
                val bitmap = BitmapFactory.decodeFile(intent.getStringExtra("file"))
                startSave(bitmap, true)
            }else{
                setResult(Activity.RESULT_OK)
                finish()
            }
        }
        loadAd()
        facebook.setOnClickListener {
            shareOnFacebook()
        }
        whatsapp.setOnClickListener {
            shareOnWhatsApp()
        }
        instagram.setOnClickListener {
            shareOnInsta()
        }
        twitter.setOnClickListener {
            shareOnTwitter()
        }
        other.setOnClickListener {
            startChooser()
        }
        eidt.setOnClickListener {
            startActivity(Intent(this, MyEditingActivity::class.java).putExtra("imageUri", intent.getStringExtra("file")))
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK){
            if (requestCode == CutOut.CUTOUT_ACTIVITY_REQUEST_CODE.toInt()){
                try {
                    val uri = CutOut.getUri(data)
                    val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, uri)
                    imageView.setImageBitmap(bitmap)
                } catch (e: Exception) {
                    e.printStackTrace()
                    Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    override fun onClick(p0: View?) {

    }

    override fun onItemResourcesClick(pre: Int, position: Int) {

    }

    override fun initializeResources() {
        setContentView(com.example.camera.R.layout.layout_preview_share)
    }

    private fun shareOnWhatsApp() {
        val imageUri = Uri.parse(intent.getStringExtra("file"))
        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND
        //Target whatsapp:
        shareIntent.setPackage("com.whatsapp")
        //Add text and then Image URI
        shareIntent.putExtra(Intent.EXTRA_STREAM, imageUri)
        shareIntent.type = "image/jpeg"
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        try {
            startActivity(shareIntent)
        } catch (ex: android.content.ActivityNotFoundException) {
            Toast.makeText(this, "Not Found", Toast.LENGTH_LONG).show()
            startChooser()
        }
    }

    private fun shareOnFacebook() {
        try {
            var facebookAppFound = false
            val fbIntent = Intent(Intent.ACTION_SEND)
            fbIntent.type = "image/jpeg"
            val uri = getUri(intent.getStringExtra("file"))
            fbIntent.putExtra(Intent.EXTRA_STREAM, uri)
            fbIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            val matches = packageManager.queryIntentActivities(intent, 0)
            fbIntent.setPackage("com.facebook.katana")
            startActivity(fbIntent)
//            for (info in matches) {
//                if (info.activityInfo.packageName.toLowerCase().startsWith("com.facebook.katana")) {
//                    fbIntent.setPackage(info.activityInfo.packageName)
//                    facebookAppFound = true
//                    break
//                }
//            }
//            if (facebookAppFound) {
//                try {
//                    startActivity(fbIntent)
//                } catch (e: Exception) {
//                    Toast.makeText(this, "You don't seem to have Facebook installed on this device", Toast.LENGTH_SHORT).show();
//                    e.printStackTrace()
//                    startChooser()
//                    return
//                }
//            }
        } catch (e: Exception) {
            Toast.makeText(this, "You don't seem to have Facebook installed on this device", Toast.LENGTH_SHORT).show();
            e.printStackTrace()
            startChooser()
        }
    }


    private fun shareOnTwitter() {
        try{
        val uri = getUri(intent.getStringExtra("file"))
        val intent =  Intent(Intent.ACTION_SEND);
        intent.setType("/*")
        intent.setClassName("com.twitter.android", "com.twitter.android.PostActivity")
        intent.putExtra(Intent.EXTRA_STREAM, uri)
        startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(this, "You don't seem to have twitter installed on this device", Toast.LENGTH_SHORT).show();
            startChooser()
        } catch (e: FileNotFoundException) {
            startChooser()
            e.printStackTrace();
        }
    }

    private fun shareOnInsta(){
        try {
            val uri = getUri(intent.getStringExtra("file"))
            val iIntent =  Intent(Intent.ACTION_SEND);
            iIntent.setType("/*")
            iIntent.putExtra(Intent.EXTRA_STREAM, uri)
            iIntent.setPackage("com.instagram.android");
            startActivity(intent)
        } catch (e: Exception) {
            e.printStackTrace()
            startChooser()
            Toast.makeText(this, "You don't seem to have Instagram installed on this device", Toast.LENGTH_SHORT).show();
        }
    }

    private fun startChooser(){
        try {
            val mIntent = Intent(Intent.ACTION_SEND)
            val uri = getUri(intent.getStringExtra("file"))
            mIntent.setType("image/jpeg")
            mIntent.putExtra(Intent.EXTRA_STREAM, uri)
            startActivity(Intent.createChooser(mIntent, "Share image using"));
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}