package com.example.camera.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.camera.Adaptors.AdapterChild;
import com.example.camera.Adaptors.CategoryModelAdapter;
import com.example.camera.BaseActivity;
import com.example.camera.R;
import com.example.camera.ads.InterstitialAds;
import com.example.camera.listeners.RecyclerItemClickListener;
import com.example.camera.models.CategoryModel;
import com.example.camera.models.CategoryModelForCameraScreen;
import com.example.camera.utils.AddTextDialog;
import com.example.camera.utils.Constants;
import com.example.camera.utils.HelloIconEvent;
import com.example.camera.utils.MyStickerView;
import com.github.gabrielbb.cutout.CutOut;
import com.jaredrummler.android.colorpicker.ColorPickerDialogListener;
import com.max.crop.mc.CropImageView;
import com.xiaopo.flying.sticker.BitmapStickerIcon;
import com.xiaopo.flying.sticker.DeleteIconEvent;
import com.xiaopo.flying.sticker.DrawableSticker;
import com.xiaopo.flying.sticker.FlipHorizontallyEvent;
import com.xiaopo.flying.sticker.Sticker;
import com.xiaopo.flying.sticker.TextSticker;
import com.xiaopo.flying.sticker.ZoomIconEvent;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MyEditingActivity extends BaseActivity implements CropImageView.OnCropImageCompleteListener,
        RecyclerItemClickListener, RecyclerItemClickListener.RecyclerItemChild, AddTextDialog.DialogListener,
        ColorPickerDialogListener {
    private ImageView effects,frames ,mirror, erase, flip, crop, blur, save, back;
    private boolean effectFlag = false,frameFlag = false, mirrorFlag = false, flipFlag = false, cropFlag = false,
    stickerFlag = false, textStickerFlag=false;
    private boolean isInChild = false;
    private Bitmap originalBitmap;
    private Bitmap currentBitmap;
    private Bitmap effectBitmap, frameBitmap;
    private MyStickerView stickerView;
    private TextSticker textSticker;
    private Sticker sticker;
    private ImageView sti;
    private ImageView txt;
    private CategoryModelAdapter adapterCategory;
    private ProgressDialog progressDialog;
    private ConstraintLayout viewToCapture;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_editing);
        initializeResources();
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Moving");
        progressDialog.setCancelable(false);
        String a = getIntent().getStringExtra("imageUri");
        InputStream inputStream = null;
        try {
            inputStream = getContentResolver().openInputStream(Uri.parse(a));
//            Uri uri = FileProvider.getUriForFile(this, "com.example.camera", new File(a));
        } catch (Exception e) {
            e.printStackTrace();
        }
        originalBitmap = BitmapFactory.decodeFile(new File(a).getAbsolutePath());
//        InputStream imagePath = Constants.stream;
//        originalBitmap = BitmapFactory.decodeStream(imagePath);
        currentBitmap = originalBitmap;
        if (currentBitmap != null){
            previewImage.setImageBitmap(originalBitmap);
        }
    }

    @Override
    public void initializeResources() {
        effects = findViewById(R.id.effects);
        mirror = findViewById(R.id.mirror);
        erase = findViewById(R.id.erase);
        flip = findViewById(R.id.flip);
        crop = findViewById(R.id.crop);
        blur = findViewById(R.id.blur);
        undo = findViewById(R.id.undo);
        redo = findViewById(R.id.redo);
//        progressBar = findViewById(R.id.progress_bar);
        txt = findViewById(R.id.text_sticker);
        sti = findViewById(R.id.sticker);
        undo.setEnabled(false);
        redo.setEnabled(false);
        redo.setAlpha(0.5f);
        undo.setAlpha(0.5f);
        stickerView = findViewById(R.id.sticker_view);
        recyclerBack = findViewById(R.id.recycler_back);
        cropImageView = findViewById(R.id.crop_image);
        back = findViewById(R.id.back);
        effectImageView = findViewById(R.id.effect_image);
        frameImageView = findViewById(R.id.frame_image);
        viewToCapture = findViewById(R.id.view_to_capture);
        frames = findViewById(R.id.frames);
//        back = findViewById(R.id.back);
        save = findViewById(R.id.save);
        previewImage = findViewById(R.id.image_view);
        recyclerFramesAndEffects = findViewById(R.id.recycler_view_effects);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerFramesAndEffects.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        effects.setOnClickListener(this);
        mirror.setOnClickListener(this);
        erase.setOnClickListener(this);
        flip.setOnClickListener(this);
        crop.setOnClickListener(this);
        blur.setOnClickListener(this);
        save.setOnClickListener(this);
        undo.setOnClickListener(this);
        redo.setOnClickListener(this);
        back.setOnClickListener(this);
        frames.setOnClickListener(this);
        recyclerBack.setOnClickListener(this);
        sti.setOnClickListener(this);
        txt.setOnClickListener(this);
        curTool = CURRENT_TOOL.DEF;
        stickerView.setOnStickerOperationListener(stickerView);
        BitmapStickerIcon deleteIcon = new BitmapStickerIcon(ContextCompat.getDrawable(this,
                com.xiaopo.flying.sticker.R.drawable.sticker_ic_close_white_18dp),
                BitmapStickerIcon.LEFT_TOP);
        deleteIcon.setIconEvent(new DeleteIconEvent());

        BitmapStickerIcon zoomIcon = new BitmapStickerIcon(ContextCompat.getDrawable(this,
                com.xiaopo.flying.sticker.R.drawable.sticker_ic_scale_white_18dp),
                BitmapStickerIcon.RIGHT_BOTOM);
        zoomIcon.setIconEvent(new ZoomIconEvent());

        BitmapStickerIcon flipIcon = new BitmapStickerIcon(ContextCompat.getDrawable(this,
                com.xiaopo.flying.sticker.R.drawable.sticker_ic_flip_white_18dp),
                BitmapStickerIcon.RIGHT_TOP);
        flipIcon.setIconEvent(new FlipHorizontallyEvent());

        BitmapStickerIcon heartIcon =
                new BitmapStickerIcon(ContextCompat.getDrawable(this, R.drawable.ic_favorite_white_24dp),
                        BitmapStickerIcon.LEFT_BOTTOM);
        heartIcon.setIconEvent(new HelloIconEvent());
        stickerView.setIcons(Arrays.asList(deleteIcon, zoomIcon, flipIcon, heartIcon));
        stickerView.setLocked(false);
        stickerView.setConstrained(true);
    }
    private int adCounter = 0;
    private ArrayList<CategoryModelForCameraScreen> list = new ArrayList<>();
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.effects:{
                if (curTool != null && curTool != CURRENT_TOOL.EFFECTS){
                    curTool = CURRENT_TOOL.EFFECTS;
                    updateFlags(0);
                    list = Constants.listEffects(this);
                    setAdapter(list, false);
                    return;
                }
                hideLayout();
                break;
            }
            case R.id.frames:{
                if (curTool != null && curTool != CURRENT_TOOL.FRAMES){
                    curTool = CURRENT_TOOL.FRAMES;
                    updateFlags(1);
                    list = Constants.listFrames(this);
                    setAdapter(list, false);
                    return;
                }
                hideLayout();
                break;
            }
            case R.id.mirror:{
                if (curTool != null && curTool != CURRENT_TOOL.MIRRORS){
                    curTool = CURRENT_TOOL.MIRRORS;
                    updateFlags(2);
                    setAdapter(mirrors);
                    return;
                }
                hideLayout();
                break;
            }
            case R.id.erase:{
                hideLayout();
//                progressBar.setVisibility(View.VISIBLE);
                progressDialog.show();
                startSave(currentBitmap, true);
                break;
            }
            case R.id.flip:{
                if (curTool != null && curTool != CURRENT_TOOL.FLIP){
                    curTool = CURRENT_TOOL.FLIP;
                    updateFlags(3);
                    setAdapter(flips);
                    return;
                }
                hideLayout();
                break;
            }
            case R.id.crop:{
                if (curTool != null && curTool != CURRENT_TOOL.CROP){
                    curTool = CURRENT_TOOL.CROP;
                    updateFlags(4);
                    setAdapter(crops);
                    crop();
                    return;
                }
                hideLayout();
                break;
            }
            case R.id.blur:{
//                progressDialog.show();
                startSave(currentBitmap, false);
//                addIntoUndo(new ModelUndoRedo(currentBitmap, effectBitmap, frameBitmap, sticker, textSticker));
//                currentBitmap = stack(currentBitmap, 50, false);
//                setCurrentBitmap(currentBitmap);
//                startActivityForResult(new Intent(this, BlurActivity.class), 439);
                break;
            }
            case R.id.undo:{
                if (adCounter > 0){
                    adCounter--;
                }
                addRedo(new ModelUndoRedo(currentBitmap, effectBitmap,frameBitmap ,sticker, textSticker));
                ModelUndoRedo model = undo();
                currentBitmap = model.getImageBitmap();
                effectBitmap = model.getEffectBitmap();
                frameBitmap = model.getFrameBitmap();
                setCurrentBitmap(currentBitmap);
                setEffect();
                setFrame();
                break;
            }
            case R.id.redo:{
                addUndo(new ModelUndoRedo(currentBitmap, effectBitmap,frameBitmap ,sticker, textSticker));
                ModelUndoRedo model = redo();
                currentBitmap = model.getImageBitmap();
                effectBitmap = model.getEffectBitmap();
                frameBitmap = model.getFrameBitmap();
                setCurrentBitmap(currentBitmap);
                setEffect();
                setFrame();
                break;
            }
            case R.id.save:{
                if (isInCrop){
                    startCropping();
                } else {
//                    previewImage.setImageBitmap(saveFinalImage(currentBitmap, effectBitmap));
//                    previewImage.setImageBitmap(saveFinalImage(viewToCapture));
                    saveFinalImage(viewToCapture);
                    stickerView.setVisibility(View.GONE);
                    effectImageView.setVisibility(View.GONE);
                }
                break;
            }
            case R.id.recycler_back:{
                hideChildRecyclerView();
                break;
            }
            case R.id.sticker:{
                if (curTool != null && curTool != CURRENT_TOOL.STICKER){
                    curTool = CURRENT_TOOL.STICKER;
                    updateFlags(5);
                    list = Constants.listStickers(this);
                    setAdapter(list, true);
                    return;
                }
                hideLayout();
                break;
            }
            case R.id.text_sticker:{
                if (curTool != null && curTool != CURRENT_TOOL.TEXT_STICKER){
                    curTool = CURRENT_TOOL.TEXT_STICKER;
                    updateFlags(6);
                    textStickerDialog();
                    return;
                }
                hideLayout();
                break;
            }
            case R.id.back:{
                finish();
                break;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        progressBar.setVisibility(View.GONE);
        progressDialog.hide();
        if (resultCode == Activity.RESULT_OK){
            if (requestCode == CutOut.CUTOUT_ACTIVITY_REQUEST_CODE){
                Uri uri = CutOut.getUri(data);
                try {
                    addIntoUndo(new ModelUndoRedo(currentBitmap, effectBitmap, frameBitmap, sticker, textSticker));
                    currentBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                    if (adCounter >= 3){
                        adCounter = 0;
                        if (InterstitialAds.Companion.isAdLoaded(this, Constants.interstitialAdId)){
                            InterstitialAds.Companion.showAd(this, Constants.interstitialAdId);
                        }
                    }else {
                        adCounter++;
                    }
                    setCurrentBitmap(currentBitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (requestCode == blurCode){
                addIntoUndo(new ModelUndoRedo(currentBitmap, effectBitmap, frameBitmap, sticker, textSticker));
                if (Constants.bitmap != null){
                    currentBitmap = Constants.bitmap;
                    setCurrentBitmap(currentBitmap);
                }
            }
        }
    }

    @Override
    public void onItemResourcesClick(int previous, int current) {
        notify(previous, current);
        decideOperation(current);

    }

    private void decideOperation(int position){
        if(mirrorFlag){
            addIntoUndo(new ModelUndoRedo(currentBitmap, effectBitmap,frameBitmap,sticker, textSticker));
            currentBitmap = mirrorOperation(position, originalBitmap);
            setCurrentBitmap(currentBitmap);
        }else if (flipFlag){
            addIntoUndo(new ModelUndoRedo(currentBitmap, effectBitmap,frameBitmap, sticker, textSticker));
            currentBitmap = flipOperation(90, currentBitmap);
            setCurrentBitmap(currentBitmap);
        }else if(cropFlag){
            Point p = croppingRatio(position);
            if (p != null){
                cropImageView.setAspectRatio(p.x, p.y);
            }else {
                cropImageView.setFixedAspectRatio(false);
            }
        }
        if (adCounter>=3){
            adCounter = 0;
            if (InterstitialAds.Companion.isAdLoaded(this, Constants.interstitialAdId)){
                InterstitialAds.Companion.showAd(this, Constants.interstitialAdId);
            }
        }else {
            adCounter++;
        }
    }

    private void crop(){
        isInCrop = true;
        cropImageView.setVisibility(View.VISIBLE);
        cropImageView.setOnCropImageCompleteListener(this);
        previewImage.setVisibility(View.GONE);
        cropImageView.setImageBitmap(currentBitmap);
    }

    private void updateFlags(int pos){
        if (pos == 0){
            effectFlag = true;
            frameFlag = false;
            mirrorFlag = false;
            flipFlag = false;
            cropFlag = false;
            stickerFlag = false;
            textStickerFlag = false;
        }else if (pos == 1){
            effectFlag = false;
            frameFlag = true;
            mirrorFlag = false;
            flipFlag = false;
            cropFlag = false;
            stickerFlag = false;
            textStickerFlag = false;
        }else if (pos == 2){
            effectFlag= false;
            frameFlag = false;
            mirrorFlag = true;
            flipFlag = false;
            cropFlag = false;
            stickerFlag = false;
            textStickerFlag = false;
        }else if (pos == 3){
            effectFlag= false;
            frameFlag = false;
            mirrorFlag = false;
            flipFlag = true;
            cropFlag = false;
            stickerFlag = false;
            textStickerFlag = false;
        }else if (pos == 4){
            effectFlag= false;
            frameFlag = false;
            mirrorFlag = false;
            flipFlag = false;
            cropFlag = true;
            stickerFlag = false;
            textStickerFlag = false;
        } else if (pos == 5){
            effectFlag= false;
            frameFlag = false;
            mirrorFlag = false;
            flipFlag = false;
            cropFlag = false;
            stickerFlag = true;
            textStickerFlag = false;
        } else if (pos == 6){
            effectFlag= false;
            frameFlag = false;
            mirrorFlag = false;
            flipFlag = false;
            cropFlag = false;
            stickerFlag = false;
            textStickerFlag = true;
        }
    }

    @Override
    public void onBackPressed() {
        if (isInCrop){
            disAbleCrop();
        }else if (isInRecycler){
            hideLayout();
        }else {
            showDialog();
        }
    }
    private void showDialog(){
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Exit?");
        dialog.setMessage("Exit editing photo?");
        dialog.setPositiveButton("Yes", (a,b)->{
            finish();
        });
        dialog.setNegativeButton("No", (a,b)->{

        });
        dialog.show();
    }

    @Override
    public void onCropImageComplete(CropImageView view, CropImageView.CropResult result) {
        if (result != null){
            if (result.getBitmap() != null){
                addIntoUndo(new ModelUndoRedo(currentBitmap, effectBitmap, frameBitmap, sticker, textSticker));
                currentBitmap = result.getBitmap();
                disAbleCrop();
                setCurrentBitmap(currentBitmap);
            }
        }
    }
    private void startCropping(){
        cropImageView.getCroppedImageAsync();
    }
    private void setAdapter(ArrayList<CategoryModelForCameraScreen> list, boolean flag){
        isInRecycler = true;
        recyclerView.setVisibility(View.GONE);
        recyclerFramesAndEffects.setVisibility(View.VISIBLE);
        recyclerBack.setVisibility(View.GONE);
        adapterCategory = new CategoryModelAdapter(this, list, this, this, flag);
        recyclerFramesAndEffects.setAdapter(adapterCategory);
    }

    @Override
    public void onItemClick(int prePos, int position) {
        if (effectFlag || frameFlag) {
            if (position == 0){
                if (effectFlag){
                    if (effectBitmap != null){
                        addIntoUndo(new ModelUndoRedo(currentBitmap, effectBitmap, frameBitmap, sticker, textSticker));
                        effectImageView.setImageBitmap(null);
                        effectImageView.setVisibility(View.GONE);
                        effectBitmap = null;
                    }
                } else {
                    if (frameBitmap != null){
                        addIntoUndo(new ModelUndoRedo(currentBitmap, effectBitmap, frameBitmap, sticker, textSticker));
                        frameImageView.setImageBitmap(null);
                        frameImageView.setVisibility(View.GONE);
                        frameBitmap = null;
                    }
                }
                return;
            }
        }
        CategoryModelForCameraScreen model;
        if (effectFlag){
            model = Constants.listEffects(this).get(position);
        }else if (frameFlag){
            model = Constants.listFrames(this).get(position);
        }else {
            model = Constants.listStickers(this).get(position);
        }
        if (adapterCategory != null){
            adapterCategory.setPrePos(-1);
        }
//        listFiles.addAll(getChildFiles(file));
        getChildFiles(model, prePos, position);
//        if ( == 0){
//            move("effects");
//            Toast.makeText(this, "No Items Downloaded", Toast.LENGTH_SHORT).show();
//        }else {
//            setChildren();
//        }
    }
    private void getChildFiles(CategoryModelForCameraScreen model, int prePos, int position){
        if (prePos != -1 && prePos == position){
            if (prePos < list.size()){
                CategoryModelForCameraScreen item = list.get(prePos);
                item.setSelected(false);
                adapterCategory.notifyItemChanged(prePos);
                adapterCategory.setPrePos(-1);
                return;
            }
        }
        File file = model.getFilePath();
        File[] files = file.listFiles();
        ArrayList<File> lis = new ArrayList<>();
        for (File f : files){
            lis.add(f);
        }
        if (lis.size()>0){
            if (prePos != -1){
                if (prePos < list.size()){
                    CategoryModelForCameraScreen item = list.get(prePos);
                    item.setSelected(false);
                    list.set(prePos, item);
                    adapterCategory.notifyItemChanged(prePos);
                }
            }
            CategoryModelForCameraScreen item = list.get(position);
            item.setListChild(lis);
            item.setSelected(true);
            list.set(position, item);
            isInChild = true;
            adapterCategory.notifyItemChanged(position);
            adapterCategory.setPrePos(position);
        }else {
            if (effectFlag){
                move("effects");
            }else if (frameFlag){
                move("frames");
            }else {
                move("stickers");
            }
            Toast.makeText(this, "No Items Downloaded", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onChildClick(File file) {
        if (effectFlag){
            addIntoUndo(new ModelUndoRedo(currentBitmap, effectBitmap, frameBitmap,sticker, textSticker));
            effectBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
            setEffect(effectBitmap);
        } else if (frameFlag){
            addIntoUndo(new ModelUndoRedo(currentBitmap,effectBitmap, frameBitmap, sticker, textSticker));
            frameBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
            setFrame(frameBitmap);
        }else {
            DrawableSticker d = fileToSticker(file.getAbsolutePath());
            stickerView.addSticker(d);
        }
        if (adCounter >= 3){
            adCounter = 0;
            if (InterstitialAds.Companion.isAdLoaded(this, Constants.interstitialAdId)){
                InterstitialAds.Companion.showAd(this, Constants.interstitialAdId);
            }
        }else {
            adCounter++;
        }
    }

    /* committed on 1/25/2020 at 11:34 AM    @Override
        public void onChildClick(int position) {
            File f = listFiles.get(position);
            if (effectFlag){
                addIntoUndo(new ModelUndoRedo(currentBitmap, effectBitmap, sticker, textSticker));
                effectBitmap = BitmapFactory.decodeFile(listFiles.get(position).getAbsolutePath());
                setEffect(effectBitmap);
            }else {
                DrawableSticker d = fileToSticker(f.getAbsolutePath());
                stickerView.addSticker(d);
            }
            if (adCounter > 3){
                adCounter = 0;
                if (InterstitialAds.Companion.isAdLoaded(this, Constants.interstitialAdId)){
                    InterstitialAds.Companion.showAd(this, Constants.interstitialAdId);
                }
            }else {
                adCounter++;
            }
        }*/
    private void hideChildRecyclerView(){
        isInChild = false;
        recyclerBack.setVisibility(View.GONE);
        recyclerFramesAndEffects.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
//        Dali.create(this).load(originalBitmap).blurRadius(12).into(effectImageView);
    }

    private void textStickerDialog(){
        new AddTextDialog(this, R.style.CustomDialog, this).show();
//        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
//        EditText input = new EditText(this);
//        dialog.setView(input);
//        dialog.setPositiveButton("Done",(v,a)-> {
//            if (!input.getText().equals("")){
//
//            }
//        });
//        dialog.setNegativeButton("Cancel",(v,a)-> {
//
//        });
//        dialog.show();
    }
    private void setEffect(){
        if (effectBitmap != null){
            setEffect(effectBitmap);
        }else {
            effectImageView.setVisibility(View.GONE);
        }
    }
    private void setFrame(){
        if (frameBitmap != null){
            setFrame(frameBitmap);
        }else {
            frameImageView.setVisibility(View.GONE);
        }

    }

    @Override
    public void okay(@NotNull String text, int color, int size, boolean isBold, boolean isUnderline, boolean isItalic) {
        Drawable bubble = ContextCompat.getDrawable(this, R.drawable.rectrangle);
        SpannableString spannableString;
        String a = text;
        spannableString = new SpannableString(a);
        if (isBold || isItalic || isUnderline) {
            if (isBold && isItalic && isUnderline) {
                spannableString.setSpan(new StyleSpan(Typeface.BOLD), 0, a.length(), 0);
                spannableString.setSpan(new StyleSpan(Typeface.ITALIC), 0, a.length(), 0);
                a = "<b><i><u>" + a + "</u></i></b>";
            } else {
                if (isBold) {
                    if (isUnderline) {
                        //bold and underline
                        a = "<b><u>" + a + "</u></b>";
                    } else if (isItalic) {
                        //bold and italic
                        a = "<b><i>" + a + "</i></b>";
                    } else {
                        //bold only
                        a = "<b>" + a + "</b>";
                    }
                } else if (isUnderline) {
                    if (isItalic) {
                        //italic and underline
                        a = "<i><u>" + a + "</u></i>";
                    } else {
                        //underline only
                        a = "<u>" + a + "</u>";
                    }
                } else {
                    //italic only
                    a = "<i>" + a + "</i>";
                }
            }
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
            a = String.valueOf(Html.fromHtml(a, Html.FROM_HTML_MODE_LEGACY));
        }else {
            a = String.valueOf(Html.fromHtml(a));
        }
        Log.v("printText", spannableString.toString());
        stickerView.addSticker(new TextSticker(getApplicationContext()).
                setDrawable(bubble)
                .setText(spannableString.toString())
                .setTextColor(textStickerColor)
                .resizeText());
    }

    @Override
    public void onColorSelected(int dialogId, int color) {
        Log.v("colorSelected", String.valueOf(color));
        this.textStickerColor = color;
    }

    @Override
    public void onDialogDismissed(int dialogId) {

    }
    int textStickerColor = -16777216;
}
