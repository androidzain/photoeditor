package com.example.camera.Activities;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;

import com.example.camera.BaseActivity;
import com.example.camera.R;
import com.example.camera.utils.BlurView;
import com.example.camera.utils.SimpleImageView;

import java.util.Objects;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

public class MyTestingBlur extends BaseActivity {
    private SeekBar radiusSeekbar;
    private SimpleImageView actualImage;
    private ImageView doneTop;
    private ConstraintLayout parent;
    private Button done;
    BlurView blurView;
    private int radius;
    private int originalRadius;
    Bitmap bitmap;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeResources();
        bitmap = BitmapFactory.decodeFile(getIntent().getStringExtra("save"));
        radiusSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (seekBar.getProgress() == 0){
                    radius = originalRadius;
                }else {
                    if ((radius - seekBar.getProgress()) < 0){
                        radius = originalRadius;
                    }else {
                        radius = radius - seekBar.getProgress();
                    }
                }
                blurView.setRadius(radius);
            }
        });
        parent.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (!a){
                    a = true;
                    Log.v("ratioFirst", ""+ ((float)(bitmap.getWidth())/(float) (bitmap.getHeight())));
                    Log.v("heightView", ""+parent.getHeight());
                    Log.v("heightBit", ""+bitmap.getHeight());
                    if (bitmap.getHeight() > parent.getHeight()){
                        int h = parent.getHeight();
                        float rat = ((float)(bitmap.getWidth())/(float) (bitmap.getHeight()));
                        int w = (int) (h * rat);
                        bitmap = Bitmap.createScaledBitmap(bitmap, w, h, true);
                    }
//                    Log.v("width", bitmap.getWidth() + " screenWidth: " + heightAndWidth());
//                    Log.v("ratioFirst", ""+ ((float)(bitmap.getWidth())/(float) (bitmap.getHeight())));
//                    if (bitmap.getWidth() >= heightAndWidth()){
//                        float ratio = ((float)(bitmap.getHeight()) / (float) (bitmap.getWidth()));
//                        int wid = heightAndWidth();
//                        int hei =(int) (wid * ratio);
//                        bitmap = Bitmap.createScaledBitmap(bitmap, wid, hei, true);
//                    }
                    Log.v("ratioSecond", ""+ ((float)(bitmap.getWidth())/(float) (bitmap.getHeight())));
                    actualImage.setBitmap(bitmap);
                    actualImage.setImageBitmap(bitmap);
                    Bitmap blur = stack(bitmap, 57, false);
                    Log.v("ratioBlur", ""+ ((float)(blur.getWidth())/(float) (blur.getHeight())));
                    if (bitmap.getWidth() > bitmap.getHeight()){
                        radius = bitmap.getHeight();
                    }else {
                        radius = bitmap.getWidth();
                    }
                    originalRadius = radius;
                    Log.v("blurWidth", blur.getWidth() + " bitmapWidth: " + bitmap.getWidth());
                    blurView.setBitmap(Objects.requireNonNull(blur), radius);
                    blurView.setImageBitmap(blur);
                }
            }
        });
    }
    boolean a = false;
    @Override
    public void initializeResources() {
        setContentView(R.layout.my_blur_layout);
        actualImage = findViewById(R.id.actual_image);
        blurView = findViewById(R.id.blur_view);
        radiusSeekbar = findViewById(R.id.seek_bar);
        done = findViewById(R.id.button);
        doneTop = findViewById(R.id.done_top);
        parent = findViewById(R.id.container);
    }

    public static Bitmap stack(Bitmap sentBitmap, int radius, boolean canReuseInBitmap) {
        // Stack Blur Algorithm by Mario Klingemann <mario@quasimondo.com>
        Bitmap bitmap;
        if (canReuseInBitmap) {
            bitmap = sentBitmap;
        } else {
            bitmap = sentBitmap.copy(sentBitmap.getConfig(), true);
        }

        if (radius < 1) {
            return (null);
        }

        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        int[] pix = new int[w * h];
        bitmap.getPixels(pix, 0, w, 0, 0, w, h);

        int wm = w - 1;
        int hm = h - 1;
        int wh = w * h;
        int div = radius + radius + 1;

        int r[] = new int[wh];
        int g[] = new int[wh];
        int b[] = new int[wh];
        int rsum, gsum, bsum, x, y, i, p, yp, yi, yw;
        int vmin[] = new int[Math.max(w, h)];

        int divsum = (div + 1) >> 1;
        divsum *= divsum;
        int dv[] = new int[256 * divsum];
        for (i = 0; i < 256 * divsum; i++) {
            dv[i] = (i / divsum);
        }

        yw = yi = 0;

        int[][] stack = new int[div][3];
        int stackpointer;
        int stackstart;
        int[] sir;
        int rbs;
        int r1 = radius + 1;
        int routsum, goutsum, boutsum;
        int rinsum, ginsum, binsum;

        for (y = 0; y < h; y++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            for (i = -radius; i <= radius; i++) {
                p = pix[yi + Math.min(wm, Math.max(i, 0))];
                sir = stack[i + radius];
                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);
                rbs = r1 - Math.abs(i);
                rsum += sir[0] * rbs;
                gsum += sir[1] * rbs;
                bsum += sir[2] * rbs;
                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }
            }
            stackpointer = radius;

            for (x = 0; x < w; x++) {

                r[yi] = dv[rsum];
                g[yi] = dv[gsum];
                b[yi] = dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (y == 0) {
                    vmin[x] = Math.min(x + radius + 1, wm);
                }
                p = pix[yw + vmin[x]];

                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[(stackpointer) % div];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi++;
            }
            yw += w;
        }
        for (x = 0; x < w; x++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            yp = -radius * w;
            for (i = -radius; i <= radius; i++) {
                yi = Math.max(0, yp) + x;

                sir = stack[i + radius];

                sir[0] = r[yi];
                sir[1] = g[yi];
                sir[2] = b[yi];

                rbs = r1 - Math.abs(i);

                rsum += r[yi] * rbs;
                gsum += g[yi] * rbs;
                bsum += b[yi] * rbs;

                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }

                if (i < hm) {
                    yp += w;
                }
            }
            yi = x;
            stackpointer = radius;
            for (y = 0; y < h; y++) {
                // Preserve alpha channel: ( 0xff000000 & pix[yi] )
                pix[yi] = (0xff000000 & pix[yi]) | (dv[rsum] << 16) | (dv[gsum] << 8) | dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (x == 0) {
                    vmin[y] = Math.min(y + r1, hm) * w;
                }
                p = x + vmin[y];

                sir[0] = r[p];
                sir[1] = g[p];
                sir[2] = b[p];

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[stackpointer];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi += w;
            }
        }
        bitmap.setPixels(pix, 0, w, 0, 0, w, h);
        return (bitmap);
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onItemResourcesClick(int pre, int position) {

    }
    private int heightAndWidth(){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        return width;
    }
}
