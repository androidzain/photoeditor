package com.example.camera.Activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.example.camera.R
import com.example.camera.ads.InterstitialAds
import com.example.camera.utils.Constants
import com.google.android.gms.ads.InterstitialAd
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings

class Splash: AppCompatActivity() {
    lateinit var rConfig : FirebaseRemoteConfig
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_splash)
        rConfig = FirebaseRemoteConfig.getInstance()
//        val settings = FirebaseRemoteConfigSettings.Builder().setMinimumFetchIntervalInSeconds(500).build()
//        rConfig.setConfigSettingsAsync(settings)
        rConfig.setDefaultsAsync(R.xml.r_config)
        getValues()
        InterstitialAds.loadAd(this, Constants.interstitialAdId)
        Handler().postDelayed({
            if (InterstitialAds.isAdLoaded(this@Splash, Constants.interstitialAdId)) {
                InterstitialAds.showAd(this@Splash, Constants.interstitialAdId)
            }
            startActivity(Intent(this, MyMainActivity::class.java).putExtra("save", "/storage/emulated/0/removedImage.png"))
            finish()
        }, 5000)
    }

    private fun getValues() {
        Constants.interstitialAdId = rConfig.getString("interstitialAd")
        Constants.interstitialAdId = rConfig.getString("bannerAd")
    }

}