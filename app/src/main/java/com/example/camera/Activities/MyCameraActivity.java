package com.example.camera.Activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PointF;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.example.camera.Adaptors.AdapterChild;
import com.example.camera.Adaptors.CategoryModelAdapter;
import com.example.camera.BaseActivity;
import com.example.camera.R;
import com.example.camera.ads.InterstitialAds;
import com.example.camera.listeners.RecyclerItemClickListener;
import com.example.camera.models.CategoryModelForCameraScreen;
import com.example.camera.utils.Constants;
import com.example.camera.utils.SharedPrefs;
import com.otaliastudios.cameraview.CameraException;
import com.otaliastudios.cameraview.CameraListener;
import com.otaliastudios.cameraview.CameraOptions;
import com.otaliastudios.cameraview.CameraView;
import com.otaliastudios.cameraview.PictureResult;
import com.otaliastudios.cameraview.VideoResult;
import com.otaliastudios.cameraview.controls.Flash;

import java.io.File;
import java.util.ArrayList;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MyCameraActivity extends BaseActivity implements RecyclerItemClickListener,
RecyclerItemClickListener.RecyclerItemChild {

    private Button buttonCapture;
    private CameraView camera;
    private TextView frameButton;
    private TextView effectButton;
    private TextView timerText;
    private boolean isEffects = false;
    private CategoryModelAdapter adapterCategory;
    private AdapterChild adapterChild;
    private ImageView maskEffect, maskFrame;
    private RecyclerView recyclerViewChild;
    private ImageView cameraButton;
    private ImageView backButton;
    private ImageView flashButton;
    private ImageView galleryButton;
    private ImageView settingsButton;
    private ImageView optionsButton;
    private ImageView timerButton;
    private ImageView swapButton, gift;
    private ArrayList<CategoryModelForCameraScreen> list;
    private String pathFrame;
    private String pathEffect;
    private boolean isEffectApplied = false;
    private boolean isFrameApplied = false;
    private boolean inChild = false;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeResources();
        frameButton.setTextColor(getResources().getColor(R.color.springGreen));
        frameOrEffect(true);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
//            if (requestCode == 545){
//                Toast.makeText(this, "Granted", Toast.LENGTH_SHORT).show();
//            }
//        }
        boolean valid = true;
        for (int grantResult : grantResults) {
            valid = valid && grantResult == PackageManager.PERMISSION_GRANTED;
        }
        if (valid && !camera.isOpened()) {
            SharedPrefs.getInstance(this).savePref(Constants.isFirstTime, true);
            camera.open();
        }
    }

    private boolean isFlash = false;
    private boolean isFront = false;

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.effects:{
                frameOrEffect(true);
                break;
            }
            case R.id.frames:{
                frameOrEffect(false);
                break;
            }
//            case R.id.rectangle:{
//                break;
//            }
            case R.id.cameraBtn:{
                if (!isTimerEnabled){
                    camera.takePicture();
                }else {
                    playTimer();
                }
                break;
            }
            case R.id.backBtn:{
                if (InterstitialAds.Companion.isAdLoaded(this, Constants.interstitialAdId)){
                    InterstitialAds.Companion.showAd(this, Constants.interstitialAdId);
                }
                super.onBackPressed();
                break;
            }
            case R.id.flashAutoBtn:{
                if (!isFlash){
                    camera.setFlash(Flash.TORCH);
                }else {
                    camera.setFlash(Flash.OFF);
                }
                isFlash = !isFlash;
                break;
            }
            case R.id.camGalleryBtn:{
                startActivityForResult(new Intent(this, GalleryActivity.class),321);
                break;
            }
            case R.id.settingsBtn:{
                startActivity(new Intent(this, SettingsActivity.class));
                break;
            }
            case R.id.menuBtn:{
                break;
            }
            case R.id.timer0Btn:{
                popUpMenu(timerButton);
                break;
            }
            case R.id.frontCamera:{
                if (camera.isTakingPicture() || camera.isTakingVideo()) return;
                Toast.makeText(this, "Switching Camera", Toast.LENGTH_SHORT).show();
                camera.toggleFacing();
                break;
            }
            case R.id.gift:{
                move("effects");
                break;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 321){
            if (resultCode == RESULT_OK){
                finish();
            }
        }
    }

    @Override
    public void initializeResources() {
        setContentView(R.layout.layout_my_camera);
        camera = findViewById(R.id.cameraView);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerViewChild = findViewById(R.id.recyclerView_files);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        recyclerViewChild.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        effectButton = findViewById(R.id.effects);
        frameButton = findViewById(R.id.frames);
        maskEffect = findViewById(R.id.mask_effect);
        maskFrame = findViewById(R.id.mask_frame);
        cameraButton = findViewById(R.id.cameraBtn);
        backButton = findViewById(R.id.backBtn);
        flashButton = findViewById(R.id.flashAutoBtn);
        galleryButton = findViewById(R.id.camGalleryBtn);
        settingsButton = findViewById(R.id.settingsBtn);
        optionsButton = findViewById(R.id.menuBtn);
        timerButton = findViewById(R.id.timer0Btn);
        swapButton = findViewById(R.id.frontCamera);
        gift = findViewById(R.id.gift);
        timerText = findViewById(R.id.timer);
        cameraButton.setOnClickListener(this);
        effectButton.setOnClickListener(this);
        frameButton.setOnClickListener(this);
        backButton.setOnClickListener(this);
        flashButton.setOnClickListener(this);
        galleryButton.setOnClickListener(this);
        settingsButton.setOnClickListener(this);
        optionsButton.setOnClickListener(this);
        timerButton.setOnClickListener(this);
        swapButton.setOnClickListener(this);
        gift.setOnClickListener(this);
        camera.setLifecycleOwner(this);
        camera.addCameraListener(new Listener());
        camera.setPlaySounds(SharedPrefs.getInstance(this).getBooleanPref(Constants.sound, true));
        if (SharedPrefs.getInstance(this).getBooleanPref(Constants.isFirstTime)){
            camera.open();
            if (SharedPrefs.getInstance(this).getBooleanPref(Constants.frontCamera)){
                camera.toggleFacing();
            }
        }
    }

    private void setAdapterChild(ArrayList<File> list){
        recyclerViewChild.setVisibility(View.VISIBLE);
        adapterChild = new AdapterChild(this, list, this);
        recyclerViewChild.setAdapter(adapterChild);
    }

    @Override
    public void onItemResourcesClick(int pre, int position) {
        if (pre == 0){
            frameButton.setTextColor(getResources().getColor(R.color.springGreen));
            effectButton.setTextColor(getResources().getColor(R.color.white));
        }else {
            frameButton.setTextColor(getResources().getColor(R.color.white));
            effectButton.setTextColor(getResources().getColor(R.color.springGreen));
        }
    }


    private void frameOrEffect(boolean a){
        if (a){
            isEffects = true;
            effectButton.setTextColor(getResources().getColor(R.color.springGreen));
            frameButton.setTextColor(getResources().getColor(R.color.white));
            list = Constants.listEffects(this);
        }else {
            isEffects = false;
            effectButton.setTextColor(getResources().getColor(R.color.white));
            frameButton.setTextColor(getResources().getColor(R.color.springGreen));
            list = Constants.listFrames(this);
        }
        recyclerView.setVisibility(View.VISIBLE);
        recyclerViewChild.setVisibility(View.GONE);
        inChild = false;
        adapterCategory = new CategoryModelAdapter(this, list, this, this, false);
        adapterCategory.setPrePos(-1);
        recyclerView.setAdapter(adapterCategory);
    }

    @Override
    public void onItemClick(int prePos, int position) {
        if (position == 0){
            if (prePos != -1){
                if (prePos < list.size()){
                    CategoryModelForCameraScreen item = list.get(prePos);
                    item.setSelected(false);
                    adapterCategory.notifyItemChanged(prePos);
                }
            }
            if (isEffects){
                pathEffect = null;
                isEffectApplied = false;
                maskEffect.setImageBitmap(null);
                maskEffect.setVisibility(View.GONE);
            }else {
                pathFrame = null;
                isFrameApplied = false;
                maskFrame.setImageBitmap(null);
                maskFrame.setVisibility(View.GONE);
            }
            return;
        }
        showChilds(prePos, position);
    }

    @Override
    public void onChildClick(File position) {
//        if (position < listChild.size()){
            if (isEffects){
                isEffectApplied = true;
//                maskEffect.setImageURI(Uri.parse(listChild.get(position).getAbsolutePath()));
//                maskEffect.setImageBitmap(BitmapFactory.decodeFile(listChild.get(position).getAbsolutePath()));
                if (maskEffect.getVisibility() == View.GONE){
                    maskEffect.setVisibility(View.VISIBLE);
                }
                maskEffect.setImageBitmap(BitmapFactory.decodeFile((position).getAbsolutePath()));
//                pathEffect = listChild.get(position).getAbsolutePath();
                pathEffect = (position).getAbsolutePath();
            }else {
                isFrameApplied = true;
//                maskFrame.setImageURI(Uri.parse(listChild.get(position).getAbsolutePath()));
//                maskFrame.setImageBitmap(BitmapFactory.decodeFile(listChild.get(position).getAbsolutePath()));
                if (maskFrame.getVisibility() == View.GONE){
                    maskFrame.setVisibility(View.VISIBLE);
                }
                maskFrame.setImageBitmap(BitmapFactory.decodeFile((position).getAbsolutePath()));
//                pathFrame = listChild.get(position).getAbsolutePath();
                pathFrame =position.getAbsolutePath();
            }
//        }
    }
    private void showChilds(int prePos, int position){
        if (prePos != -1 && prePos == position){
            if (prePos < list.size()) {
                CategoryModelForCameraScreen item = list.get(prePos);
                item.setSelected(false);
                list.set(prePos, item);
                adapterCategory.notifyItemChanged(prePos);
                adapterCategory.setPrePos(-1);
                return;
            }
        } else if (prePos != -1){
            CategoryModelForCameraScreen item = list.get(prePos);
            item.setSelected(false);
            list.set(prePos, item);
            adapterCategory.notifyItemChanged(prePos);
        }
        CategoryModelForCameraScreen item = list.get(position);
        populateChilds(item, position, item.getFilePath());
    }
    private void populateChilds(CategoryModelForCameraScreen model, int pos, File dir){
        File[] lis = dir.listFiles();
        ArrayList<File> files = new ArrayList<>();
        for (File f : lis){
            files.add(f);
        }
        if (files.size() > 0){
            model.setListChild(files);
//            listChild = files;
            model.setSelected(true);
            list.set(pos, model);
            adapterCategory.notifyItemChanged(pos);
            adapterCategory.setPrePos(pos);
//            inChild = true;
//            childFiles(files);
        }else {
            Toast.makeText(this, "No Items Downloaded Yet", Toast.LENGTH_SHORT).show();
            if (isEffects){
                move("effects");
            }else {
                move("frames");
            }
//            inChild = false;
//            if (!isEffects){
//                move("frames");
//            }else {
//                move("effects");
//            }
//            Toast.makeText(this, "No Item Found", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        if (inChild){
            inChild = false;
            recyclerViewChild.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }else {
            super.onBackPressed();
        }
    }

    private class Listener extends CameraListener {

        @Override
        public void onCameraOpened(@NonNull CameraOptions options) {

        }

        @Override
        public void onCameraError(@NonNull CameraException exception) {
            super.onCameraError(exception);

        }

        @Override
        public void onPictureTaken(@NonNull PictureResult result) {
            super.onPictureTaken(result);
            if (camera.isTakingVideo()) {
                return;
            }
            result.toBitmap((bitmap)->{
                Bitmap eff=null;
                Bitmap fra=null;
                if (isEffectApplied) {
                    if (pathEffect != null && !pathEffect.equals("")) {
                        eff = BitmapFactory.decodeFile(pathEffect);
                    }
                }
                if (isFrameApplied) {
                    if (pathFrame != null && !pathFrame.equals("")) {
                        fra = BitmapFactory.decodeFile(pathFrame);
                    }
                }
                saveFinalImage(bitmap, eff, fra);
            });
        }

        @Override
        public void onVideoTaken(@NonNull VideoResult result) {
            super.onVideoTaken(result);
        }

        @Override
        public void onVideoRecordingStart() {
            super.onVideoRecordingStart();
        }

        @Override
        public void onVideoRecordingEnd() {
            super.onVideoRecordingEnd();
        }

        @Override
        public void onExposureCorrectionChanged(float newValue, @NonNull float[] bounds, @Nullable PointF[] fingers) {
            super.onExposureCorrectionChanged(newValue, bounds, fingers);
        }

        @Override
        public void onZoomChanged(float newValue, @NonNull float[] bounds, @Nullable PointF[] fingers) {
            super.onZoomChanged(newValue, bounds, fingers);
        }
    }

    private CountDownTimer timer;
    int a;
    private void playTimer(){
        a  = (int) time;
        timerText.setVisibility(View.VISIBLE);
        timerText.setText(String.valueOf(a));
        timer = new CountDownTimer(time * 1000, 1000) {
            @Override
            public void onTick(long l) {
                a--;
                timerText.setText(String.valueOf(a));
            }

            @Override
            public void onFinish() {
                timerText.setVisibility(View.GONE);
                camera.takePicture();
            }
        };timer.start();
    }
}
