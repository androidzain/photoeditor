package com.example.camera;


import android.content.Context;

public class SharedPreference {

    //name
    public static String IMAGE_DATA = "IMAGE_DATA";
    public static String COLLAGE_IMAGE = "COLLAGE_IMAGE";


    // keys
    public static String ImgFromGallery = "ImgFromGallery";
    public Context context;
    public static String ImageUri = "imageUri";
    public static String CameraImageBitMap = "CameraImageBitMap";
    public static String TextViewImage = "TextViewImage";


    //Settings
    //NAME
    public static String SETTINGS_PREFS = "SETTINGS_PREFS";

    //KEY
    public static String START_FROM_HOME = "START_FROM_HOME";
    public static String FRONT_CAMERA = "FRONT_CAMERA";
    public static String DIRECT_SAVE = "DIRECT_SAVE";
    public static String MIRROR_EFFECT = "MIRROR_EFFECT";
    public static String SOUND = "SOUND";
    public static String WATER_MARK = "WATER_MARK";


}
