package com.example.camera;

import android.app.Application;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;

import io.alterac.blurkit.BlurKit;

public class CameraApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        MobileAds.initialize(this);
        AdRequest.Builder builder = new AdRequest.Builder();
        builder.addTestDevice("3763D3312CABF08E1B316085EAD7D56E");
        BlurKit.init(this);
    }
}
