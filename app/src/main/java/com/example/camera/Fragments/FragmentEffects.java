package com.example.camera.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.camera.Adaptors.CategoriesAdapter;
import com.example.camera.Model.ModelCategories;
import com.example.camera.R;
import com.example.camera.utils.Constants;
import java.util.ArrayList;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

public class FragmentEffects extends BaseFragment {

    public FragmentEffects() {
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.fragment_layout, container, false);
        initializeResources(view);
        return view;
    }

    private void initializeResources(View view) {
        recyclerView = view.findViewById(R.id.recyclerView);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        list = new ArrayList<>();
        list.addAll(Constants.getDataEffects());
        for (int i = 0; i<list.size(); i++){

        }
        recyclerView.setLayoutManager(layoutManager);
        adapter = new CategoriesAdapter(list, this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onItemClick(int pre, int position) {
        move(list.get(position).getList(), Constants.dirEffects);
    }
}
