package com.example.camera.Fragments;

import android.content.Intent;

import com.example.camera.Activities.DownloadingActivity;
import com.example.camera.Adaptors.CategoriesAdapter;
import com.example.camera.Model.ModelCategories;
import com.example.camera.Model.ModelImage;
import com.example.camera.listeners.RecyclerItemClickListener;
import com.example.camera.utils.Constants;

import java.util.ArrayList;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public abstract class BaseFragment extends Fragment implements RecyclerItemClickListener {
    public RecyclerView recyclerView;
    public CategoriesAdapter adapter;
    public GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 3);
    public ArrayList<ModelCategories> list;
    public void move(ArrayList<ModelImage> list, String category){
        Constants.curDir = category;
        getContext().startActivity(new Intent(getContext(), DownloadingActivity.class).putExtra("samples_list", list)
        .putExtra("dir", category));
    }
}
