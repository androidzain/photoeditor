package com.example.camera.listeners;

import java.io.File;

public interface RecyclerItemClickListener {
    public void onItemClick(int prePos,int position);
    interface RecyclerItemChild{
        public void onChildClick(File file);
    }

    interface RecyclerListener {
        public void onItemResourcesClick(int pre, int position);
    }
}
